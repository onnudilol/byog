from .base import FunctionalTest

from selenium.webdriver.common.keys import Keys


class PartsListTest(FunctionalTest):

    def test_parts_list_displays_parts(self):
        # Partario is a red-blooded American and wants to build his own rifle.
        # He starts by browsing parts lists on BYOG which has mysteriously risen out of the ether.
        # The home page menu displays menu items for each of the relevant parts.  Partario checks for lowers.

        self.browser.get(self.server_url)
        self.browser.find_element_by_link_text('Lowers').send_keys(Keys.RETURN)

        # The page lists several different types of lowers.
        # Partario decides to look at a PSA lower.

        self.browser.find_element_by_link_text('Palmetto State Armory Liberty Lower').send_keys(Keys.RETURN)

        # The page displays detailed specs on the part.
