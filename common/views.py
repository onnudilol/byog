from django.views.generic.base import TemplateView
from django.db.models import Count

from articles.models import Article
from lists.models import ItemList


class HomePageView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['articles'] = Article.objects.select_related('articlecoverimage', 'author').all().order_by('-date')[:4]
        context['latest'] = ItemList.objects.filter(complete=True).prefetch_related('user', 'buildimage_set',
                                                                                    'buildvote_set').annotate(
            score=Count('buildvote')).order_by('-date')[:4]
        context['featured'] = ItemList.objects.filter(complete=True, featured=True).prefetch_related('user',
                                                                                                     'buildimage_set',
                                                                                                     'buildvote_set').annotate(
            score=Count('buildvote')).order_by('-date')[:4]

        return context


class TOSView(TemplateView):
    template_name = 'tos.html'
