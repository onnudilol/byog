from common.templatetags import deslugify, markdown_tags, replace_whitespace
from unittest import TestCase


class DeslugifyTest(TestCase):

    def test_deslugify(self):
        deslug = deslugify.deslugify('this-is-a-string_that_has_hyphens_and_underscores')
        assert deslug == 'this is a string that has hyphens and underscores'


class MarkDownTest(TestCase):

    def test_markdown_to_html(self):
        md = markdown_tags.mark_down('#header')
        self.assertEqual(md, '<h1 id="header">header</h1>')

    def test_markdown_escapes_blacklisted_tags(self):
        md = markdown_tags.mark_down('<script>console.log("helo")</script>')
        self.assertEqual(md, '&lt;script&gt;console.log("helo")&lt;/script&gt;')


class ReplaceWhitespaceTest(TestCase):

    def test_whitespace_gets_replaced_with_plus_by_default(self):
        plus = replace_whitespace.replace_whitespace('a string with whitespace')
        self.assertEqual('a+string+with+whitespace', plus)

    def test_whitespace_gets_replaced_with_arbitrary_symbols(self):
        semicolon = replace_whitespace.replace_whitespace('a string with whitespace', ';')
        self.assertEqual('a;string;with;whitespace', semicolon)
