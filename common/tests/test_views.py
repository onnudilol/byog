from django.test import TestCase
from django.contrib.auth import get_user_model
from django.utils import timezone

from articles.models import Article
from lists.models import ItemList, BuildVote

User = get_user_model()


class HomePageTest(TestCase):
    def test_home_page_view_uses_correct_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_page_fetches_latest_articles(self):
        author = User.objects.create(username='h')

        for i in range(1, 6):
            Article.objects.create(title='Article {}'.format(i), author=author)

        old = Article.objects.last()

        response = self.client.get('/')
        self.assertNotIn(old, response.context['articles'])

    def test_home_page_fetches_latest_featured_builds(self):
        author = User.objects.create(username='h')

        for i in range(1, 6):
            ItemList.objects.create(title='Build {}'.format(i), deck=i, complete=True, featured=True,
                                    user=author, date=timezone.now())

        old = ItemList.objects.first()

        response = self.client.get('/')
        self.assertNotIn(old, response.context['featured'])

    def test_home_page_fetches_latest_complete_builds(self):
        author = User.objects.create(username='h')

        for i in range(1, 6):
            ItemList.objects.create(title='Build {}'.format(i), deck=i, complete=True,
                                    user=author, date=timezone.now())

        old = ItemList.objects.first()

        response = self.client.get('/')
        self.assertNotIn(old, response.context['latest'])

    def test_home_page_annotates_votes_to_build(self):
        author = User.objects.create(username='h')

        for i in range(1, 6):
            build = ItemList.objects.create(title='Build {}'.format(i), deck=i, complete=True, featured=True,
                                            user=author, date=timezone.now())
            BuildVote.objects.create(user=author, build=build)

        response = self.client.get('/')
        self.assertEqual(response.context['latest'][0].score, 1)


class TOSTest(TestCase):
    def test_tos_page_view_uses_correct_template(self):
        response = self.client.get('/tos/')
        self.assertTemplateUsed(response, 'tos.html')
