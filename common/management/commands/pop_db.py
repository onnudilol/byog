from django.core.management.base import BaseCommand
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

from parts.models import Part, PriceBase, PartImage

import json
import requests
from random import randint
from time import sleep


class Command(BaseCommand):
    help = 'Populates the database with data from input json'

    def add_arguments(self, parser):
        parser.add_argument('input-json', help='json file with which to populate the db')
        parser.add_argument('vendor', choices=['PA', 'PSA', 'MUSA', 'BRWN'], help='vendor of scraped json data')
        parser.add_argument('--save-pic', action='store_true')

    def handle(self, *args, **options):
        part_counter = 0
        price_counter = 0
        price_update_counter = 0

        with open(options['input-json']) as i:
            parts = json.load(i)

            for part in parts:
                default_part = {
                    'name': part['name'],
                    'category': part['category'],
                    'manufacturer': part['manufacturer'],
                    'product_num': part.get('sku'),
                    'specs': part.get('specs', {})
                }
                default_part['specs'].pop('brand', None)

                default_price = {
                    'price': part.get('price'),
                    'url': part['url'],
                }

                if part['sku']:
                    obj_part, part_created = Part.objects.get_or_create(product_num=part['sku'], defaults=default_part)

                # fallback for some sites of questionable quality that don't have skus listed
                else:
                    obj_part, part_created = Part.objects.get_or_create(name=default_part['name'], defaults=default_part)

                # update category
                if obj_part.category != default_part['category']:
                    obj_part.category = default_part['category']
                    obj_part.save()
                    print('Category updated: {}'.format(obj_part.name))

                # update manufacturer
                if obj_part.manufacturer != default_part['manufacturer']:
                    obj_part.manufacturer = default_part['manufacturer']
                    obj_part.save()
                    print('Manufactuer updated: {}'.format(obj_part.name))

                obj_price, price_created = PriceBase.objects.get_or_create(part=obj_part, vendor=options['vendor'],
                                                                           defaults=default_price)

                # update price
                if default_price['price']:
                    if obj_price.price != default_price['price']:
                        obj_price.price = default_price['price']
                        obj_price.save()
                        print('Price updated: {}'.format(obj_part.name))
                        price_update_counter += 1

                # update price url
                if obj_price.url != default_price['url']:
                    obj_price.url = default_price['url']
                    obj_price.save()
                    print('URL updated: {}'.format(obj_part.name))

                if options['save_pic']:
                    # Based on https://djangosnippets.org/snippets/2587/
                    req = requests.get(part['img'])

                    if req.status_code != requests.codes.ok:
                        continue

                    img_temp = NamedTemporaryFile(delete=True)
                    img_temp.write(req.content)
                    img_temp.flush()

                    PartImage.objects.create(part=obj_part, image=File(img_temp))

                    sleep(randint(1, 100))

                if part_created:
                    part_counter += 1
                if price_created:
                    price_counter += 1
                    if part_created is False:
                        price_update_counter += 1
                        print('Added price to existing item: {}: {} @ {}'.format(obj_part.name, obj_price.price,
                                                                                 obj_price.vendor))
                    else:
                        print('Added price {}: {} @ {}'.format(obj_part.name, obj_price.price, obj_price.vendor))

        print('{} items added'.format(part_counter))
        print('{} prices added'.format(price_counter))
        print('{} prices updated'.format(price_update_counter))
