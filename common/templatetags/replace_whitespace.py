import re
from django import template


register = template.Library()


@register.filter()
def replace_whitespace(string, replace='+'):
    return string.replace(' ', replace)
