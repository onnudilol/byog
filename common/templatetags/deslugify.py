# Modified from https://gist.github.com/airtonix/1603781
import re
from django import template


register = template.Library()


@register.filter(name='deslugify')
def deslugify(value):
    """
    Cleans up a slug by removing slug separator characters that occur at the
    beginning or end of a slug.
    """

    reverse_map = {
      "_": " ",
      "-": " ",
    }

    for _from, _to in reverse_map.items():
        value = re.sub(_from, _to, value)

    return value
