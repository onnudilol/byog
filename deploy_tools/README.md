# Passwords
* Do not generate passwords with double quotation marks (")

# Manual Intervention Required
* Need to manually create DNS A record
* Need to manually create DNS Mx records for Zoho
* No command for populating the db using management commands yet
* Manually request ssl cert with letsencrypt

# Manual Intervention Optional
* If you're planning to use virtualenvwrapper, you should manually create the virtualenv