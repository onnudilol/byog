from django.contrib import admin
from lists.models import ItemList, Item, CustomItem, Build, BuildImage


class ItemInline(admin.TabularInline):
    model = Item
    raw_id_fields = ['part']
    exclude = ['name']
    extra = 3


class CustomItemInline(admin.TabularInline):
    model = CustomItem
    extra = 3


class BuildImageInline(admin.TabularInline):
    exclude = ['user']
    model = BuildImage
    extra = 3


class ItemListAdmin(admin.ModelAdmin):
    readonly_fields = ['slug']
    inlines = [ItemInline, CustomItemInline]
    raw_id_fields = ['user']
    search_fields = ['user__username', 'title']
    list_display = ['title', 'user', 'active']
    list_filter = ['active']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(complete=False)


class BuildAdmin(admin.ModelAdmin):
    readonly_fields = ['slug']
    inlines = [BuildImageInline]
    raw_id_fields = ['user']
    search_fields = ['user__username', 'title']
    list_display = ['title', 'user', 'featured']
    list_filter = ['featured']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(complete=True)


admin.site.register(ItemList, ItemListAdmin)
admin.site.register(Build, BuildAdmin)
