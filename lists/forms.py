from django import forms
from django.forms.widgets import TextInput

from lists.models import ItemList, CustomItem, BuildImage, LIST_MAX_PRICE
from parts.part_info.vendors import VENDOR_CHOICES

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Submit
from crispy_forms.bootstrap import InlineRadios


class ListDetailForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # crispy-forms settings
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Fieldset(
                'Edit {{ title }}',
                'title',
            )
        )
        self.helper.add_input(Submit('submit', 'Save'))

    class Meta:
        model = ItemList
        fields = ['title']

    def update(self, the_list):
        # Just updates the title
        the_list.title = self.cleaned_data.get('title')
        the_list.save()


class EditItemForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # crispy-forms settings
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Fieldset(
                'Edit {{ item }}',
                InlineRadios('choose_vendor'),
                'vendor',
                InlineRadios('choose_price'),
                'custom_price'
            )
        )
        self.helper.add_input(Submit('submit', 'Save'))

    choose_vendor = forms.ChoiceField(choices=((True, 'Use price from selected vendor:'),),
                                      label='', required=False)
    vendor = forms.ChoiceField(choices=VENDOR_CHOICES, label='', required=False)
    choose_price = forms.ChoiceField(choices=((True, 'Manually enter a price:'),),
                                     label='', required=False)
    custom_price = forms.DecimalField(max_value=9999.99, label='', required=False)

    def save(self, item, price, total_price, old_price=None, old_custom_price=None):

        # If there is a custom price, save the custom price
        if self.cleaned_data['custom_price']:

            if old_custom_price:
                if total_price + (self.cleaned_data['custom_price'] - old_custom_price) > LIST_MAX_PRICE:
                    return False

            elif self.cleaned_data['custom_price'] + total_price > LIST_MAX_PRICE:
                return False

            item.custom_price = self.cleaned_data['custom_price']
            item.save()
            return item

        # If there is a selected vendor, set the item vendor and price, and clear the custom price
        elif self.cleaned_data['choose_vendor']:
            if price:

                if old_price:
                    if total_price + (price - old_price) > LIST_MAX_PRICE:
                        return False

                elif total_price + price > LIST_MAX_PRICE:
                    return False

            item.vendor = self.cleaned_data['vendor']
            item.price = price
            item.custom_price = None
            item.save()
            return item

    # If the user chooses to enter a custom price but does not enter one, the form is unclean
    # If the user chooses to enter a custom price and select a price from a vendor, the form is unclean
    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data['custom_price'] and cleaned_data['custom_price'] < 0:
            cleaned_data['custom_price'] = abs(cleaned_data['custom_price'])

        if cleaned_data.get('choose_price') and not cleaned_data.get('custom_price'):
            raise forms.ValidationError('Enter a custom price.', code='no_price')

        if cleaned_data.get('choose_price') and cleaned_data.get('choose_vendor'):
            raise forms.ValidationError('Choose one or the other.', code='both_options')


class CustomItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # crispy-forms settings
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Fieldset(
                '{% if item %}Edit {{ item }}{% else %}Add Custom Item{% endif %}',
            )
        )
        self.helper.add_input(Submit('submit', 'Save'))

    price = forms.DecimalField(max_value=9999.99)

    class Meta:
        model = CustomItem
        fields = ['category', 'part', 'price', 'url']
        widgets = {'url': TextInput}
        labels = {'url': 'URL (Optional)'}

    # Saves a custom item to the selected list
    def save(self, the_list):
        return CustomItem.objects.create(itemlist=the_list, category=self.cleaned_data['category'],
                                         part=self.cleaned_data['part'], price=self.cleaned_data['price'],
                                         url=self.cleaned_data['url'])

    # Updates a custom item's fields, excluding the list field
    def update(self, item_id, old_price, total_price):
        if self.cleaned_data['price'] != old_price:
            if total_price + (self.cleaned_data['price'] - old_price) > LIST_MAX_PRICE:
                return False

        updated_item = CustomItem.objects.filter(id=item_id).update(category=self.cleaned_data['category'],
                                                                    part=self.cleaned_data['part'],
                                                                    price=self.cleaned_data['price'],
                                                                    url=self.cleaned_data['url'])
        return updated_item

    # From http://www.tangowithdjango.com/book/chapters/forms.html#cleaner-forms
    def clean(self):
        cleaned_data = super().clean()
        url = cleaned_data.get('url')

        # If url is not empty and doesn't start with 'http://', prepend 'http://'.
        if url and not url.startswith('http://'):
            url = 'http://' + url
            cleaned_data['url'] = url

        if cleaned_data['price'] < 0:
            cleaned_data['price'] = abs(cleaned_data['price'])

        return cleaned_data


class EditBuildForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # crispy-forms settings
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Fieldset(
                'Edit {{ title }}',
                'title',
                'description',
                'deck'
            )
        )
        self.helper.add_input(Submit('submit', 'Save'))

    class Meta:
        model = ItemList
        fields = ['title', 'description', 'deck']
        help_texts = {'deck': 'Enter a short description about your build.'}

    def update(self, build):
        build.title = self.cleaned_data['title']
        build.description = self.cleaned_data['description']
        build.deck = self.cleaned_data['deck']
        build.save()


class BuildImageForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_id = 'id_image_form'
        self.helper.layout = Layout(
            Fieldset(
                'Upload Images',
                'image',
            )
        )

    class Meta:
        model = BuildImage
        fields = ['image']
        labels = {'image': ''}

    def save(self, user, build, image):
        return BuildImage.objects.create(user=user, build=build, image=image)
