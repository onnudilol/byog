from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.contrib import messages
from django.http import JsonResponse, HttpResponseNotAllowed
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.db.models import Count
from django.utils import timezone

from lists.models import ItemList, Item, CustomItem, BuildImage, BuildVote, LIST_MAX_PRICE
from lists.forms import ListDetailForm, CustomItemForm, EditItemForm, EditBuildForm, BuildImageForm
from parts.models import Part, PartReview
from parts.forms import PartReviewForm
from parts.part_info.categories import CATEGORY_CHOICES_CORE, CATEGORY_CHOICES_CORE_FIELDS
from profiles.models import UserProfile
from common.sso import get_disqus_sso

from itertools import chain
import decimal
from collections import OrderedDict
import copy


def list_manage(request, slug=None):
    """
    The main view where the user manages their list.  Retrieves the currently active list and pre-fetches its related
    items and custom items.  Sorts items and custom items by primarily by category and secondarily by name
    and returns the combined and sorted list.

    Args:
        request: The incoming HttpRequest
        slug: The selected list's slug (optional).  If not present, fetch the user's active list.

    Returns:
        site: The full domain of the site
        list: The user's active list
        items: The combined items and custom items, sorted by category
    """

    active_list = None
    other_items = None
    site = None
    manage = False
    categories = OrderedDict((category, []) for category in CATEGORY_CHOICES_CORE)

    # If there is a slug, get the list or 404
    if slug:
        active_list = get_object_or_404(ItemList.objects.prefetch_related('item_set__part', 'customitem_set'), slug=slug)

    # Otherwise, retrieve the user's active list
    elif not slug and request.user.is_authenticated():

        # Get the active list, pre-fetch items and custom items.
        # The active list queryset should only return a single object.  filter() is used instead of get() in order
        # to prefetch the items and custom items.
        active_list = ItemList.objects.filter(active=True, user=request.user).prefetch_related('item_set__part',
                                                                                               'customitem_set').first()

        # Don't want the server to freak out if the user doesn't have an active list.  Ignore the exception and render
        # the template telling the user to start a list.
        if not active_list:
            manage = True

    # Sort items and custom items by category and name into combined list.
    if active_list:
        items = active_list.item_set.all()
        custom_items = active_list.customitem_set.all()
        combined_items = sorted(chain(items, custom_items), key=lambda i: (i.category, str(i)))
        core_items = list()

        # Extract the items that are part of the core categories
        for item in combined_items:

            if item.get_category_display() in categories:
                categories[item.get_category_display()].append(item)
                core_items.append(item)

        # Remove items from combined_items that were added to core_items
        other_items = list(set(combined_items) - set(core_items))

        site = "http://{}".format(Site.objects.get_current().domain) + active_list.get_absolute_url()

        if active_list.user == request.user:
            manage = True

    return render(request, 'lists/list.html', {'navbar': 'lists',
                                               'site': site,
                                               'list': active_list,
                                               'other_items': other_items,
                                               'categories': categories,
                                               'category_fields': CATEGORY_CHOICES_CORE_FIELDS,
                                               'list_manage': manage})


@login_required
def new_list(request):
    """
    Creates a new list and marks any currently active list as inactive.

    Args:
        request: The incoming HttpRequest

    Returns:
        The newly created list.  Redirects to the main list view.
    """

    if request.method == 'POST':

        try:
            active_list = request.user.itemlist_set.get(active=True)

        # If there are no currently active lists, ignore the exception and redirect to the main list view.
        except ObjectDoesNotExist:
            pass

        else:
            active_list.active = False
            active_list.save()

        list_count = request.user.itemlist_set.filter(complete=False).count() + 1
        title = 'My Build {}'.format(list_count)

        ItemList.objects.create(user=request.user, title=title)

        return redirect('list_manage')

    else:
        return HttpResponseNotAllowed('POST')


class ListDelConfirmation(TemplateView):
    """
    Renders a template asking user to confirm deletion of build/list
    """
    template_name = 'lists/confirm_delete.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        slug = self.kwargs['slug']
        username = self.kwargs.get('username')
        context['username'] = username
        context['slug'] = slug
        return context


@login_required
def del_list(request, slug, username=None):
    """
    Deletes the selected list.

    Args:
        request: The incoming HttpRequest
        slug: The selected list's slug.
        username: Username from profile

    Returns:
        If no username parameter, redirect to the main list view.
        Else, direct the user to their profile list or build,
        depending on whether or not the selected list was complete or not
        403 if a different user attempts to delete the list.
    """

    if request.method == 'POST':

        the_list = get_object_or_404(ItemList, slug=slug)
        complete = the_list.complete

        if request.user == the_list.user:
            the_list.delete()

            if username and complete:
                return redirect('profile_builds', username=username)
            elif username and not complete:
                return redirect('profile_lists', username=username)
            else:
                return redirect('list_manage')

        else:
            raise PermissionDenied

    else:
        return HttpResponseNotAllowed('POST')


@login_required
def edit_list(request, slug, username=None):
    """
    Edits the list title.

    Args:
        request: The incoming HttpRequest.  GET request renders the form.  POST request processes form data.
        slug: The selected list's slug.
        username: If provided, the view redirects to the user's profile

    Returns:
        form: An instance of ListDetailForm
        403 if a different user attempts to edit the list.
    """

    the_list = get_object_or_404(ItemList, slug=slug, complete=False)

    if request.user == the_list.user:
        form = None

        if request.method == 'GET':
            form = ListDetailForm(instance=the_list)

        elif request.method == 'POST':
            form = ListDetailForm(request.POST)

            if form.is_valid():
                form.update(the_list)

                if username:
                    # TODO figure out how to change this to named url
                    return redirect('/users/{}/lists/?view={}'.format(username, the_list.slug))
                else:
                    return redirect('list_manage')

        return render(request, 'lists/list_edit.html', {'form': form, 'title': the_list.title})

    else:
        raise PermissionDenied


@login_required
def open_list(request, slug):
    """
    Marks the currently active list as inactive and marks the selected list as active.

    Args:
        request: The incoming HttpRequest
        slug: The slug of the list to be marked as active

    Returns:
        Redirects to list base view
        403s if another user attempts to mark a list as active

    """

    if request.method == 'POST':

        active_list = ItemList.objects.get(slug=slug)

        if request.user == active_list.user:

            if not active_list.active:

                # If there's a current active list, set it as inactive
                # Otherwise, do nothing
                try:
                    old_list = request.user.itemlist_set.get(active=True)

                except ItemList.DoesNotExist:
                    pass

                else:
                    old_list.active = False
                    old_list.save()

                active_list.active = True
                active_list.complete = False
                active_list.save()

            return redirect('list_manage')

        else:
            raise PermissionDenied

    else:
        return HttpResponseNotAllowed('POST')


def add_item(request, part_id):
    """
    Adds the selected part to the user's active list.
    Creates a new list if the user doesn't currently have an active list.
    Handles both AJAX and regular POST requests.

    Args:
        request: The incoming HttpRequest
        part_id: The selected part's ID

    Returns:
        Redirect to the main list view.
    """

    if request.method == 'POST':

        if request.user.is_authenticated():

            active_list, created = request.user.itemlist_set.get_or_create(active=True)
            part = Part.objects.get(id=part_id)

            if part.lowest_price:
                part_price = part.lowest_price
            else:
                part_price = 0

            max_price = active_list.total_price + part_price > LIST_MAX_PRICE

            if not max_price:

                # If there are any existing items in the category, delete them and subtract from the list total price
                old_item = active_list.item_set.filter(part__category=part.category)

                if old_item:
                    for item in old_item:
                        if item.custom_price:
                            active_list.total_price -= item.custom_price

                        else:
                            try:
                                active_list.total_price -= item.price
                            # Selected item price is None
                            except TypeError:
                                pass

                        item.delete()

                item = Item.objects.create(part=part, itemlist=active_list)

                # Add the part price to the list total
                # If the part has no price, do nothing
                try:
                    active_list.total_price += item.price
                except TypeError:
                    pass

                if created:
                    list_count = request.user.itemlist_set.filter(complete=False).count()
                    title = 'My Build {}'.format(list_count)
                    active_list.title = title

                active_list.save()

            else:
                messages.error(request, 'Maximum total price exceeded.  \
                                    Please remove some items from your list.')

            # HACK Chrome doesn't like to redirect xhr, so the redirect url is dumped to json and handled by js
            if request.is_ajax():
                return JsonResponse({'redirectTo': redirect('list_manage').url})
            else:
                return redirect('list_manage')

        else:
            if request.is_ajax():
                return JsonResponse({'redirectTo': redirect('account_login').url})
            else:
                return redirect('account_login')

    else:
        return HttpResponseNotAllowed('POST')


@login_required
def add_custom_item(request):
    """
    Adds a custom item to the user's currently active list.
    Creates a new list if the user doesn't currently have an active list.

    Args:
        request: The incoming HttpRequest.  GET request renders the form.  POST request processes form data.

    Returns:
        form: An instance of CustomItemForm
    """

    form = None

    if request.method == 'GET':
        form = CustomItemForm()

    elif request.method == 'POST':
        form = CustomItemForm(request.POST)

        if form.is_valid():
            active_list, created = request.user.itemlist_set.get_or_create(active=True)

            if active_list.total_price + form.cleaned_data['price'] < LIST_MAX_PRICE:
                item = form.save(active_list)
                active_list.total_price += item.price

                if created:
                    list_count = request.user.itemlist_set.filter(complete=False).count()
                    title = 'My Build {}'.format(list_count)
                    active_list.title = title

                active_list.save()

            else:
                messages.error(request, 'Maximum total price exceeded.\
                Please remove some items from your list.')

            return redirect('list_manage')

    return render(request, 'lists/list_custom_add_edit.html', {'form': form})


@login_required
def edit_item(request, slug, item_id):
    """
    Edits an item in the list.
    The user can either select a vendor and the associated price if it exists,
    or manually enter their own price, but not both.

    Args:
        request: The incoming HttpRequest.  GET request renders the form.  POST request processes form data.
        slug: The selected list's slug
        item_id: The selected item's id

    Returns:
        form: An instance of EditItemForm, initialized with the selected item
        use_custom: Boolean that controls which form controls are enabled.  Defaults to False.
        403 if a different user attempts to edit the list.
    """

    the_list = get_object_or_404(ItemList.objects.select_related('user'), slug=slug)
    use_custom = False

    if request.user == the_list.user:
        item = get_object_or_404(Item.objects.select_related('part'), id=item_id)
        old_price = item.price if item.price else None
        old_custom_price = item.custom_price if item.custom_price else None
        total_price = the_list.total_price
        form = None

        if request.method == 'GET':
            form = EditItemForm(initial={'vendor': item.vendor, 'custom_price': item.custom_price})

            # If the item has a previously entered custom price, default to editing the custom price.
            if item.custom_price:
                use_custom = True

        if request.method == 'POST':
            form = EditItemForm(request.POST)

            if form.is_valid():
                # If the user has chosen a vendor, retrieve that vendor's price if it exists or None
                try:
                    price = item.part.pricebase_set.get(vendor=form.cleaned_data['vendor']).price

                except ObjectDoesNotExist:
                    price = None

                item = form.save(item, price, total_price, old_price, old_custom_price)

                # arithmetic for handling item price changes
                if item:
                    if item.custom_price:
                        if old_custom_price is None:
                            the_list.total_price += (item.custom_price - old_price)
                        else:
                            the_list.total_price += (item.custom_price - old_custom_price)

                    elif item.custom_price is None:
                        new_price = item.price if item.price else decimal.Decimal(0)

                        if old_custom_price:
                            the_list.total_price += (new_price - old_custom_price)
                        else:
                            if item.price != old_price:
                                the_list.total_price += (new_price - old_price)

                    the_list.save()

                else:
                    messages.error(request, 'Maximum total price exceeded.  \
                                        Please remove some items from your list.')

                return redirect('list_manage')

            # The user has chosen to input a custom price and entered invalid input.
            # Default to editing the custom price.
            else:
                use_custom = True

        return render(request, 'lists/list_item_edit.html', {'form': form, 'item': item.part,
                                                             'use_custom': use_custom})

    else:
        raise PermissionDenied


@login_required
def edit_custom_item(request, slug, item_id):
    """
    Edits a custom item.

    Args:
        request: The incoming HttpRequest.  GET request renders the form.  POST request processes form data.
        slug: The selected list's slug
        item_id: The selected item's id

    Returns:
        form: An instance of CustomItemForm
        item: The part of the custom item being edited or None
        403 if a different user attempts to edit the list.
    """

    the_list = get_object_or_404(ItemList.objects.select_related('user'), slug=slug)
    form = None

    if request.user == the_list.user:
        item = get_object_or_404(CustomItem, id=item_id)
        old_price = item.price
        total_price = the_list.total_price

        if request.method == 'GET':
            form = CustomItemForm(instance=item)
            return render(request, 'lists/list_custom_add_edit.html', {'form': form, 'item': item.part})

        elif request.method == 'POST':
            form = CustomItemForm(request.POST)

            if form.is_valid():
                updated_item = form.update(item_id, old_price, total_price)

                if updated_item:
                    the_list.total_price += (form.cleaned_data['price'] - old_price)
                    item.itemlist.save()

                else:
                    messages.error(request, 'Maximum total price exceeded.  \
                                        Please remove some items from your list.')

        return redirect('list_manage')

    else:
        raise PermissionDenied


@login_required
def del_item(request, slug, item_id):
    """
    Deletes an item.

    Args:
        request: The incoming HttpRequest
        slug: The selected list's slug
        item_id: The selected item's id

    Returns:
        Redirect to the main list view.
        403 if a different user attempts to edit the list.
    """

    if request.method == 'POST':

        the_list = get_object_or_404(ItemList, slug=slug)

        if request.user == the_list.user:
            item = get_object_or_404(Item, id=item_id)

            if item.custom_price:
                the_list.total_price -= item.custom_price

            else:
                try:
                    the_list.total_price -= item.price
                # Selected item price is None
                except TypeError:
                    pass

            the_list.save()
            item.delete()

            return redirect('list_manage')

        else:
            raise PermissionDenied

    else:
        return HttpResponseNotAllowed('POST')


@login_required
def del_custom_item(request, slug, item_id):
    """
        Deletes a custom item.

        Args:
            request: The incoming HttpRequest
            slug: The selected list's slug
            item_id: The selected custom item's id

        Returns:
            Redirect to the main list view.
            403 if a different user attempts to edit the list.
        """

    if request.method == 'POST':

        the_list = get_object_or_404(ItemList, slug=slug)

        if request.user == the_list.user:
            item = get_object_or_404(CustomItem, id=item_id)

            the_list.total_price -= item.price
            the_list.save()
            item.delete()

            return redirect('list_manage')

        else:
            raise PermissionDenied

    else:
        return HttpResponseNotAllowed('POST')


@login_required
def complete_build(request, slug):
    """
    Marks a list as a complete build and redirects the user to edit the title/description,
    if the list has at least one item or custom item.
    Otherwise display an error page.

    Args:
        request: The incoming HttpRequest
        slug: The selected list's slug

    Returns:
        Redirect to complete build edit view
        403 if another user attempts to mark the build as complete
    """

    if request.method == 'POST':

        the_list = get_object_or_404(ItemList, slug=slug)

        if request.user == the_list.user:

            # Marks the current list as inactive and complete, sets the published date,
            # and redirects user to edit the title and description
            if the_list.item_set.count() >= 1 or the_list.customitem_set.count() >= 1:
                the_list.active = False
                the_list.complete = True

                if not the_list.date:
                    the_list.date = timezone.now()

                the_list.save()
                return redirect('build_edit', slug=slug)

            # If the list doesn't have any items or custom items, redirect to the list base view with an error message
            else:
                messages.error(request,  'You must add at least one item or custom item \
                to your list before marking it as complete.')
                return redirect('list_manage')

        else:
            raise PermissionDenied

    else:
        return HttpResponseNotAllowed('POST')


@login_required
def edit_build_list(request, slug):
    """
    Marks a build as incomplete and active for editing.

    Args:
        request: The incoming HttpRequest
        slug: The selected build's slug

    Returns:
        Redirects to list_manage view.
        403 if another user tries to mark edit the build list
    """

    if request.method == 'POST':

        build = ItemList.objects.get(slug=slug)

        if request.user == build.user:

            try:
                active_list = ItemList.objects.get(user=request.user, active=True)
                active_list.active = False
                active_list.save()

            except ObjectDoesNotExist:
                pass

            build.complete = False
            build.active = True
            build.save()

            return redirect('list_manage')

        else:
            raise PermissionDenied

    else:
        return HttpResponseNotAllowed('POST')


class BuildEditConfirmation(TemplateView):
    """
    Renders a template asking user for confirmation to edit a build's list
    """

    template_name = 'lists/confirm_edit.html'


def build_manage(request, slug):
    """
    This view renders a template with links for the user to edit the build title/description, upload images,
    write part reviews, and delete the build

    Args:
        request: The incoming HttpRequest
        slug: The selected build's slug

    Returns:
        build: The selected build
        403 if another user tries to access the private build page
    """

    build = get_object_or_404(ItemList.objects.prefetch_related(
        'item_set', 'customitem_set', 'partreview_set__part', 'buildvote_set'
    ).annotate(
        score=Count('buildvote')),
        slug=slug, complete=True)
    items = build.item_set.all().prefetch_related('part').order_by('category')
    custom_items = build.customitem_set.all().order_by('category')
    site = "http://{}".format(Site.objects.get_current().domain) + build.get_absolute_url()

    user_image = None
    try:
        user_image = build.user.userprofile.user_image
    except UserProfile.DoesNotExist:
        pass

    manage = False
    if request.user == build.user:
        manage = True

    disqus = ''
    if request.user.is_authenticated:
        disqus = get_disqus_sso(request.user, site, build.slug)

    return render(request, 'lists/build.html', {'build': build,
                                                'items': items,
                                                'custom_items': custom_items,
                                                'user_image': user_image,
                                                'site': site,
                                                'disqus': disqus,
                                                'build_manage': manage})


@login_required
def edit_build(request, slug):
    """
    Renders a form to edit a build's title and description.

    Args:
        request: The incoming HttpRequest
        slug: The selected build's slug

    Returns:
        form: An instance of EditBuildForm
        Redirect to the user's completed builds
        403 if another user attempts to edit the build description
    """

    form = None
    build = get_object_or_404(ItemList, slug=slug, complete=True)

    if request.user == build.user:

        if request.method == 'GET':
            form = EditBuildForm(instance=build)

        elif request.method == 'POST':
            form = EditBuildForm(request.POST)
            if form.is_valid():
                form.update(build)
                return redirect('build_manage', slug=slug)

        return render(request, 'lists/build_edit.html', {'form': form, 'title': build.title})

    else:
        raise PermissionDenied


@login_required
def build_image(request, slug):
    """
    Allows the user to upload and delete images related to the build

    Args:
        request: The incoming HttpRequest
        slug: The selected build's slug

    Returns:
        images: The build images
        form: An instance of BuildImageForm
        Redirects back to the build images on POST
        403 if another user attempts to add or remove images from the build
    """

    build = get_object_or_404(ItemList.objects.prefetch_related('buildimage_set'), slug=slug, complete=True)
    title = build.title
    images = build.buildimage_set.all()
    form = None

    if build.user == request.user:

        if request.method == 'GET':
            form = BuildImageForm()

        elif request.method == 'POST':
            form = BuildImageForm(request.POST, request.FILES)

            if form.is_valid():
                form.save(user=request.user, build=build, image=request.FILES['image'])
                return redirect('build_image', slug=slug)

        return render(request, 'lists/build_image.html', {'form': form, 'images': images,
                                                          'slug': slug, 'title': title})

    else:
        raise PermissionDenied


@login_required
def del_build_image(request, image_id):
    """
    Deletes an image from a completed build

    Args:
        request: The incoming HttpRequest
        image_id: The selected image's id

    Returns:
        Redirect to build_image view.
        403s if another user tries to delete an image.
    """

    if request.method == 'POST':

        image = BuildImage.objects.filter(id=image_id).prefetch_related('build').first()

        if request.user == image.user:
            image.delete()
            return redirect('build_image', slug=image.build.slug)

        else:
            raise PermissionDenied

    else:
        return HttpResponseNotAllowed('POST')


@login_required
def build_review(request, slug):
    """
    Fetches all the reviews associated with a build

    Args:
        request: The incoming HttpRequest
        slug: The selected build's slug

    Returns:
        reviews: The set of a build's reviews
        403 if another user attempts to access
    """

    build = get_object_or_404(ItemList.objects.prefetch_related('item_set__partreview', 'item_set__part'),
                              slug=slug, complete=True)
    reviews = build.item_set.all()

    if request.user == build.user:
        return render(request, 'lists/build_review.html', {'reviews': reviews, 'slug': slug, 'title': build.title})

    else:
        raise PermissionDenied


@login_required
def edit_review(request, slug, item_id):
    """
    Creates a new review for a part if one doesn't exist for the user.
    Otherwise renders a form with an instance of the review to edit and overwrites the build and item field.

    Args:
        request: The incoming HttpRequest
        slug: The selected build's slug
        item_id: The selected item's ID

    Returns:
        Redirects to the build_review page.
        403s if another user tries to add or edit a review
    """

    build = get_object_or_404(ItemList, slug=slug, complete=True)

    if build.user == request.user:
        item = get_object_or_404(Item.objects.prefetch_related('part'), id=item_id)
        part = item.part

        # Renders a form with the existing review instance, or a blank form if none is found
        if request.method == 'GET':
            try:
                review = PartReview.objects.get(part=part, user=request.user)

            except PartReview.DoesNotExist:
                form = PartReviewForm()

            else:
                form = PartReviewForm(instance=review)

            return render(request, 'lists/build_review_edit.html', {'form': form, 'part': part})

        # If the review already exists, update the fields
        # Otherwise, create a new instance
        elif request.method == 'POST':
            form = PartReviewForm(request.POST)
            if form.is_valid():
                try:
                    review = PartReview.objects.get(part=part, user=request.user)

                except PartReview.DoesNotExist:
                    form.save(user=request.user, item=item, part=part, build=build)
                    return redirect('build_review', slug=slug)

                else:
                    form.update(review, item, build)
                    return redirect('build_review', slug=slug)

            else:
                return render(request, 'lists/build_review_edit.html', {'form': form, 'part': part})

    else:
        raise PermissionDenied


@login_required
def del_review(request, slug, review_id):
    """
    Deletes a review

    Args:
        request: The incoming HttpRequest
        slug: The selected build's slug
        review_id: The selected review's ID

    Returns:
        Redirects to build review page
        403 if another user tries to delete a review
    """

    if request.method == 'POST':

        review = PartReview.objects.get(id=review_id)

        if review.user == request.user:
            review.delete()
            return redirect('build_review', slug=slug)

        else:
            raise PermissionDenied

    else:
        return HttpResponseNotAllowed('POST')


@login_required
def build_vote(request, slug):
    """
    Creates a BuildVote object if the user has not voted on the build previously, or deletes a BuildVote object
    if the user has voted on the build previously.

    Args:
        request: The incoming HttpRequest.  Should be AJAX.
        slug:  The selected build's slug

    Returns:
        The updated build score
    """
    created = None

    if request.is_ajax():
        build = get_object_or_404(ItemList.objects.prefetch_related('buildvote_set'), slug=slug, complete=True)
        vote, created = BuildVote.objects.get_or_create(build=build, user=request.user)

        if not created:
            vote.delete()

    return JsonResponse({'created': created})


class BuildList(ListView):
    model = ItemList
    queryset = ItemList.objects.filter(complete=True).prefetch_related('user', 'buildimage_set',
                                                                       'buildvote_set').annotate(
        score=Count('buildvote')).order_by('-date')
    context_object_name = 'builds'
    template_name = 'lists/build_list.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['navbar'] = 'completed'
        return context


class BuildListFeatured(ListView):
    model = ItemList
    queryset = ItemList.objects.filter(complete=True, featured=True).prefetch_related('user', 'buildimage_set',
                                                                                      'buildvote_set').annotate(
        score=Count('buildvote')).order_by('-date')
    context_object_name = 'builds'
    template_name = 'lists/build_list_featured.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['navbar'] = 'featured'
        return context


def export_list(request, slug, format_type):
    """
    Formats a given list into the selected format type

    Args:
        request: The incoming HttpRequest
        slug: The slug of the selected list
        format_type: The selected format type [markdown, bbcode, html, text]

    Returns:
        A string of the list formatted into the selected format
    """

    # list_format and item_format are tuples that are unpacked to generate the strings for the table
    # The order and indices of the tuple items are below for reference
    # list_format: ([0] list.title, [1] list.get_absolute_url(), [2] list.total_price, [3] site)
    # item_format: ([0] item.get_category_display(), [1] item.part, [2] item.part.get_absolute_url(), [3] site, [4] price)

    if request.is_ajax():

        export_format = {
            'markdown': {
                'header': (
                    "### [{0}]({3}{1})\n"
                    "Category|Item|Price\n"
                    ":---|:---|---:\n"
                ),
                'footer': (
                    "| | **Total** | **${2}**\n"
                    "| | [*Created @ Gatpicker.com*]({3}) |"
                ),
                'item': "**{0}** | [{1}]({3}{2}) | {4}\n",
                'custom_item': "**{0}** | {1} | {4}\n"
            },
            'bbcode': {
                'header': "[url={3}{1}]{0}[/url]\n\n",
                'footer': (
                    "[b]Total[/b]: ${2}\n\n"
                    "[url={3}]Created @ Gatpicker.com[/url]"
                ),
                'item': "[b]{0}:[/b] [url={3}{2}]{1}[/url] - [{4}]\n",
                'custom_item': "[b]{0}[/b]: {1} - [{4}]\n",
            },
            'html': {
                'header': (
                    '<a href="{3}{1}">{0}</a>\n'
                    '<br>\n'
                    '<table>\n'
                    '    <thead>\n'
                    '        <tr>\n'
                    '            <th>Category</th>\n'
                    '            <th>Item</th>\n'
                    '            <th>Price</th>\n'
                    '        </tr>\n'
                    '    </thead>\n'
                    '    <tbody>\n'
                ),
                'footer': (
                    "        <tr>\n"
                    "            <td></td>\n"
                    "            <td><b>Total:</b> </td>\n"
                    "            <td>${2}</td>\n"
                    "        </tr>\n"
                    "        <tr>\n"
                    "            <td></td>\n"
                    '            <td><a href="={3}">Created @ Gatpicker.com</a></td>\n'
                    "            <td></td>\n"
                    "        </tr>\n"
                    "    </tbody>\n"
                    "</table>"
                ),
                'item': (
                    "        <tr>\n"
                    "            <td><b>{0}</b></td>\n"
                    "            <td><a href='{3}{2}'>{1}</a></td>\n"
                    "            <td>{4}</td>\n"
                    "        </tr>\n"
                ),
                'custom_item': (
                    "        <tr>\n"
                    "            <td><b>{0}</b></td>\n"
                    "            <td>{1}</td>\n"
                    "            <td>{4}</td>\n"
                    "        </tr>\n"
                )
            },
            'text': {
                'header': (
                    "{0}: {3}{1}\n"
                    "\n"
                ),
                'footer': (
                    "\n"
                    "Total: ${2}\n"
                    "Created @ Gatpicker.com"
                ),
                'item': "{0}: {1} - [{4}]\n",
                'custom_item': "{0}: {1} - [{4}]\n"
            }
        }

        site = "http://{}".format(Site.objects.get_current().domain)
        the_list = get_object_or_404(ItemList.objects.prefetch_related('item_set__part', 'customitem_set'), slug=slug)
        items = the_list.item_set.all()
        custom_items = the_list.customitem_set.all()
        combined_items = sorted(chain(items, custom_items), key=lambda item: (item.category, str(item)))

        list_format = (the_list.title, the_list.get_absolute_url(), the_list.total_price, site)

        export_header = export_format[format_type]['header'].format(*list_format)
        export_footer = export_format[format_type]['footer'].format(*list_format)
        export_items = ''

        for item in combined_items:

            if type(item) == Item:
                price = '-'

                if item.custom_price:
                    price = "${} → Custom Price"
                elif item.price:
                    price = "${} → {}".format(item.price, item.get_vendor_display())

                item_format = (item.get_category_display(), item.part, item.part.get_absolute_url(), site, price)
                export_items += export_format[format_type]['item'].format(*item_format)

            else:
                price = "${} → Custom Item".format(item.price)
                item_format = (item.get_category_display(), item.part, None, site, price)
                export_items += export_format[format_type]['custom_item'].format(*item_format)

        export_table = export_header + export_items + export_footer
        return JsonResponse({'table': export_table})
