# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-13 22:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0011_auto_20161006_1958'),
    ]

    operations = [
        migrations.AddField(
            model_name='itemlist',
            name='currency',
            field=models.CharField(choices=[('USD', '$')], default='USD', max_length=3),
        ),
    ]
