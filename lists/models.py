from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.dispatch import receiver
from django.utils.crypto import get_random_string

from byog.settings import AUTH_USER_MODEL
from common.models import RestrictedImageField, IMAGE_TYPES
from parts.part_info.categories import CATEGORY_CHOICES
from parts.part_info.vendors import VENDOR_CHOICES
from parts.part_info.currency import CURRENCY_CHOICES

import os
import uuid
import decimal

LIST_MAX_PRICE = decimal.Decimal(999999.99)


class ItemList(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    slug = models.CharField(unique=True, max_length=7)
    active = models.BooleanField(default=True)
    complete = models.BooleanField(default=False)
    featured = models.BooleanField(default=False)
    title = models.CharField(default='', max_length=100)
    deck = models.CharField(default='', max_length=140)
    description = models.TextField(default='', max_length=5000)
    total_price = models.DecimalField(max_digits=8, decimal_places=2, default=0, blank=True)
    currency = models.CharField(max_length=3, choices=CURRENCY_CHOICES, default='USD')
    date = models.DateTimeField(null=True)

    def save(self, *args, **kwargs):
        """
        Randomly generates a slug on creation
        """
        if not self.slug:
            self.slug = get_random_string(length=7)

        super().save(*args, **kwargs)

    def get_absolute_url(self):
        if not self.complete:
            return reverse('list_public', kwargs={'slug': self.slug})
        if self.complete:
            return reverse('build_manage', kwargs={'slug': self.slug})

    def __str__(self):
        return "{} - {}".format(self.user, self.title)


class Build(ItemList):

    class Meta:
        proxy = True


class BuildVote(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE)
    build = models.ForeignKey('ItemList', on_delete=models.CASCADE)

    class Meta:
        unique_together = (
            ('user', 'build')
        )


class Item(models.Model):
    itemlist = models.ForeignKey('ItemList', on_delete=models.CASCADE)
    part = models.ForeignKey('parts.Part', on_delete=models.CASCADE)
    # Since sorted() treats foreignkey values as strings rather than objects, it needs to be set again for the Item
    category = models.CharField(max_length=30, choices=CATEGORY_CHOICES, default='', blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    custom_price = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    vendor = models.CharField(max_length=15, choices=VENDOR_CHOICES, null=True, blank=True)

    def __str__(self):
        return str(self.part)

    def save(self, *args, **kwargs):
        """
        If the user hasn't selected a vendor, set the vendor with the lowest price
        Automatically set the item category based on the part category
        """
        if not self.vendor:
            self.vendor = self.part.lowest_price_vendor
            self.price = self.part.lowest_price
        if not self.category:
            self.category = self.part.category

        super().save(*args, **kwargs)


class CustomItem(models.Model):
    itemlist = models.ForeignKey('ItemList', on_delete=models.CASCADE)
    category = models.CharField(max_length=30, choices=CATEGORY_CHOICES)
    part = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    url = models.URLField(default='', blank=True)

    def __str__(self):
        return "{} - {}".format(self.get_category_display(), self.part)

    @property
    def name(self):
        return self.__str__()


# From http://stackoverflow.com/questions/2673647/enforce-unique-upload-file-names-using-django
def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('img/builds/', filename)


class BuildImage(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    build = models.ForeignKey('ItemList', on_delete=models.CASCADE)
    image = RestrictedImageField(upload_to=get_file_path, content_types=IMAGE_TYPES,
                                 max_upload_size=5242880, null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.user:
            self.user = self.build.user
        super().save(*args, **kwargs)


# From http://stackoverflow.com/a/16041527
@receiver(models.signals.post_delete, sender=BuildImage)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Deletes an uploaded image on disk when the associated model is deleted"""

    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


@receiver(models.signals.pre_save, sender=BuildImage)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem when corresponding buildimage object is changed."""

    if not instance.pk:
        return False

    try:
        old_image = BuildImage.objects.get(pk=instance.pk).image

    except BuildImage.DoesNotExist:
        return False

    new_image = instance.image
    if old_image:
        if not old_image == new_image:
            if os.path.isfile(old_image.path):
                os.remove(old_image.path)
