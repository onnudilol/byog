from django.test import TestCase
from django.contrib.auth import get_user_model

from lists.forms import ListDetailForm, EditItemForm, CustomItemForm, EditBuildForm
from lists.models import ItemList, Item, CustomItem, LIST_MAX_PRICE
from parts.models import Part, PriceBase

import decimal

User = get_user_model()


class ListDetailFormTest(TestCase):

    def test_update_list_details(self):
        user = User.objects.create()
        the_list = ItemList.objects.create(user=user, title='No Title')

        form = ListDetailForm(data={'title': 'The Title'})

        self.assertTrue(form.is_valid())
        form.update(the_list)

        self.assertEqual(the_list.title, 'The Title')


class EditFormTest(TestCase):

    def setUp(self):
        self.user = User.objects.create()
        self.part = Part.objects.create()
        self.the_list = ItemList.objects.create(user=self.user)
        self.item = Item.objects.create(itemlist=self.the_list, category='trigger', part=self.part, vendor='MUSA', price=500)

    def test_save_custom_price(self):
        form = EditItemForm(data={'choose_price': True, 'custom_price': 100})
        form.is_valid()
        form.save(self.item, 100, total_price=decimal.Decimal(500))

        self.assertEqual(self.item.custom_price, 100)

    def test_save_vendor_price(self):
        PriceBase.objects.create(part=self.part, vendor='PA', price=100)
        form = EditItemForm(data={'choose_vendor': True, 'vendor': 'PA'})

        self.assertTrue(form.is_valid())
        form.save(self.item, 100, total_price=decimal.Decimal(500))

        self.assertEqual(self.item.vendor, 'PA')
        self.assertEqual(self.item.price, 100)

    def test_save_vendor_price_sets_price_to_none(self):
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()

        form = EditItemForm(data={'choose_vendor': True, 'vendor': 'PA'})
        form.is_valid()
        form.save(self.item, price=None, total_price=self.the_list.total_price, old_price=500)

        item = Item.objects.get(id=self.item.id)

        self.assertIsNone(item.price)

    def test_save_custom_price_returns_false_if_list_max_price_exceeded(self):
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()

        form = EditItemForm(data={'choose_price': True, 'custom_price': 1000})
        form.is_valid()
        form_save = form.save(self.item, price=None, total_price=self.the_list.total_price, old_price=500)

        self.assertFalse(form_save)

    def test_save_custom_price_returns_false_if_old_custom_price_and_list_max_price_exceeded(self):
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()
        self.item.custom_price = 100
        self.item.save()

        form = EditItemForm(data={'choose_price': True, 'custom_price': 1000})
        form.is_valid()
        form_save = form.save(self.item, price=None, total_price=self.the_list.total_price, old_price=500)

        self.assertFalse(form_save)

    def test_save_vendor_price_returns_false_if_list_max_price_exceeded(self):
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()
        PriceBase.objects.create(part=self.part, vendor='PA', price=1000)

        form = EditItemForm(data={'choose_vendor': True, 'vendor': 'PA'})
        form.is_valid()
        form_save = form.save(self.item, price=decimal.Decimal(1000), total_price=self.the_list.total_price, old_price=500)

        self.assertFalse(form_save)

    def test_clean_data_sets_absolute_value_for_negative_price(self):
        form = EditItemForm(data={'choose_price': True, 'custom_price': -1000})
        form.is_valid()
        price = form.cleaned_data['custom_price']

        self.assertEqual(price, 1000)

    def test_clean_data_choose_price_but_no_custom_price_is_unclean(self):
        form = EditItemForm(data={'choose_price': True})
        self.assertFalse(form.is_valid())

    def test_cannot_choose_vendor_price_and_custom_price(self):
        form = EditItemForm(data={'choose_price': True, 'choose_vendor': True})
        self.assertFalse(form.is_valid())


class CustomItemFormTest(TestCase):

    def setUp(self):
        self.user = User.objects.create()
        self.the_list = ItemList.objects.create(user=self.user)

    def test_create_custom_item(self):
        form = CustomItemForm(data={'category': 'trigger',
                                    'part': 'shoot butan',
                                    'price': 100,
                                    'url': 'example.com'})

        self.assertTrue(form.is_valid())
        form.save(self.the_list)
        self.assertTrue(CustomItem.objects.first().category, 'trigger')

    def test_edit_custom_item(self):
        item = CustomItem.objects.create(itemlist=self.the_list,
                                         category='trigger',
                                         part='shoot butan',
                                         price=100,
                                         url='example.com')
        form = CustomItemForm(data={'category': 'laser',
                                    'part': 'laser beam',
                                    'price': 500,
                                    'url': 'google.com'})

        self.assertTrue(form.is_valid())
        form.update(item.id, old_price=decimal.Decimal(item.price), total_price=decimal.Decimal(100))
        item = CustomItem.objects.first()
        self.assertEqual(item.category, 'laser')

    def test_custom_item_form_clean_data_appends_http_to_url_if_not_present(self):
        item = CustomItem.objects.create(itemlist=self.the_list,
                                         category='trigger',
                                         part='shoot butan',
                                         price=100,
                                         url='example.com')
        form = CustomItemForm(data={'category': 'laser',
                                    'part': 'laser beam',
                                    'price': 500,
                                    'url': 'google.com'})
        form.is_valid()
        url = form.cleaned_data['url']
        self.assertTrue(url.startswith('http://'))

    def test_custom_item_form_clean_data_sets_absolute_value_for_negative_price(self):
        form = CustomItemForm(data={'category': 'trigger',
                                    'part': 'shoot butan',
                                    'price': -100,
                                    'url': 'example.com'})
        form.is_valid()
        price = form.cleaned_data['price']
        self.assertEqual(price, 100)

    def test_update_custom_item(self):
        item = CustomItem.objects.create(itemlist=self.the_list,
                                         category='trigger',
                                         part='shoot butan',
                                         price=100,
                                         url='example.com')
        form = CustomItemForm(data={'category': 'laser',
                                    'part': 'laser beam',
                                    'price': 500,
                                    'url': 'google.com'})
        form.is_valid()
        form.update(item.id, decimal.Decimal(100), decimal.Decimal(100))
        item = CustomItem.objects.get(id=item.id)

        self.assertEqual(item.price, 500)


class EditBuildFormTest(TestCase):

    def test_edit_build_form_update(self):
        self.user = User.objects.create()
        build = ItemList.objects.create(user=self.user, complete=True, active=False,
                                        title='Test Title', description='Test Description', deck='Test Deck')

        form = EditBuildForm(data={'title': 'New Title', 'description': 'New Description', 'deck': 'New Deck'})
        form.is_valid()
        form.update(build)
        build = ItemList.objects.get(id=build.id)
        self.assertEqual(build.description, 'New Description')
