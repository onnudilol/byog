from django.test import TestCase
from django.contrib.auth import get_user_model

from lists.models import ItemList, Item, CustomItem, LIST_MAX_PRICE
from lists.forms import ListDetailForm, EditItemForm, CustomItemForm, EditBuildForm
from parts.models import Part, PriceBase, PartReview
from parts.forms import PartReviewForm

import json


User = get_user_model()


class NewListTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.client.force_login(self.user)

    def test_user_must_authenticate_to_create_new_list(self):
        self.client.logout()
        response = self.client.post('/lists/new')

        self.assertRedirects(response, '/accounts/login/?next=/lists/new')

    def test_create_new_list_saves_owner(self):
        self.assertTrue(self.user.is_authenticated())
        self.client.post('/lists/new')

        the_list = ItemList.objects.first()

        self.assertEqual(the_list.user, self.user)

    def test_add_item_creates_new_list(self):
        part = Part.objects.create(manufacturer='a', name='slug', id=1)
        PriceBase.objects.create(part=part, price=1.11, vendor='PA', url='com.com')
        self.client.post('/lists/add_item/{}'.format(part.id))

        item = Item.objects.first()
        self.assertIn(item, ItemList.objects.first().item_set.all())

    def test_add_item_ajax_creates_new_list(self):
        part = Part.objects.create(manufacturer='a', name='slug', id=1)
        PriceBase.objects.create(part=part, price=1.11, vendor='PA', url='com.com')
        self.client.post('/lists/add_item/{}'.format(part.id),
                         content_type='application/json',
                         HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        item = Item.objects.first()
        self.assertIn(item, ItemList.objects.first().item_set.all())

    def test_must_be_authenticated_to_add_item(self):
        self.client.logout()
        part = Part.objects.create(manufacturer='a', name='slug', id=1)
        PriceBase.objects.create(part=part, price=1.11, vendor='PA', url='com.com')
        response = self.client.post('/lists/add_item/{}'.format(part.id))

        self.assertRedirects(response, '/accounts/login/')

    def test_create_new_list_sets_current_active_list_as_inactive(self):
        ItemList.objects.create(user=self.user, active=True)
        self.client.post('/lists/new')
        old_list = ItemList.objects.first()

        self.assertFalse(old_list.active)

    def test_create_new_list_redirects_to_list_manage_view(self):
        part = Part.objects.create(manufacturer='a', name='slug', id=1)
        PriceBase.objects.create(part=part, price=1.11, vendor='PA', url='com.com')
        response = self.client.post('/lists/add_item/{}'.format(part.id))

        self.assertRedirects(response, '/lists/')

    def test_create_new_list_generates_generic_title(self):
        self.client.post('/lists/new')
        self.assertEqual(ItemList.objects.first().title, 'My Build 1')

        self.client.post('/lists/new')
        self.assertEqual(ItemList.objects.last().title, 'My Build 2')

    def test_405_if_not_POST(self):
        response = self.client.get('/lists/new')
        self.assertEqual(response.status_code, 405)


class ViewListTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.the_list = ItemList.objects.create(user=self.user, active=True)

    def test_view_active_list_uses_correct_template(self):
        response = self.client.get('/lists/')
        self.assertTemplateUsed(response, template_name='lists/list.html')

    def test_specific_list_uses_correct_template(self):
        response = self.client.get('/lists/{}/'.format(self.the_list.slug))
        self.assertTemplateUsed(response, template_name='lists/list.html')

    def test_specific_list_404_if_slug_does_not_exist(self):
        response = self.client.get('/lists/1234567/')
        self.assertEqual(response.status_code, 404)

    def test_can_view_other_users_list(self):
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        the_list = ItemList.objects.create(user=self.user)

        response = self.client.get('/lists/{}/'.format(the_list.slug))
        self.assertEqual(response.status_code, 200)

    def test_fetches_active_list_if_authenticated(self):
        self.client.force_login(self.user)
        response = self.client.get('/lists/')

        self.assertEqual(self.the_list, response.context['list'])

    def test_generate_correct_permalink(self):
        self.client.force_login(self.user)
        response = self.client.get('/lists/')
        self.assertEqual('http://example.com/lists/{}/'.format(self.the_list.slug), response.context['site'])

    def test_combine_items(self):
        part1 = Part.objects.create(category='stock', name='stock')
        part2 = Part.objects.create(category='trigger', name='triger safezone')
        part3 = Part.objects.create(category='trigger', name='trigger warning')

        item1 = Item.objects.create(part=part1, itemlist=self.the_list)
        item2 = Item.objects.create(part=part2, itemlist=self.the_list)
        item3 = Item.objects.create(part=part3, itemlist=self.the_list)
        custom = CustomItem.objects.create(category='barrel', part='donkey kong', price=100, itemlist=self.the_list)

        response = self.client.get('/lists/{}/'.format(self.the_list.slug))
        self.assertEqual([custom, item1, item2, item3], response.context['items'])


class EditListTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.the_list = ItemList.objects.create(user=self.user)
        self.the_build = ItemList.objects.create(user=self.user, complete=True, active=False)
        self.client.force_login(self.user)

    def test_edit_list_details_uses_correct_form(self):
        response = self.client.get('/lists/{}/edit'.format(self.the_list.slug))
        self.assertIsInstance(response.context['form'], ListDetailForm)

    def test_edit_list_details(self):
        self.client.post('/lists/{}/edit'.format(self.the_list.slug), data={'title': 'My+List'})
        new_title = ItemList.objects.first()
        self.assertEqual(new_title.title, 'My+List')

    def test_edit_list_redirects_to_list_manage_default(self):
        response = self.client.post('/lists/{}/edit'.format(self.the_list.slug), data={'title': 'My+List'})
        self.assertRedirects(response, '/lists/')

    def test_edit_list_redirects_to_profile_if_username(self):
        response = self.client.post('/users/{}/lists/{}/edit'.format(self.user, self.the_list.slug), data={'title': 'My+List'})
        self.assertRedirects(response, '/users/{}/lists/?view={}'.format(self.user, self.the_list.slug))

    def test_cannot_edit_other_users_list_details(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/lists/{}/edit'.format(self.the_list.slug),
                                    data={'title': 'User 1 sucks',
                                          'description': 'User 2 rules'})

        self.assertEqual(response.status_code, 403)


class DelListTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.the_list = ItemList.objects.create(user=self.user)
        self.the_build = ItemList.objects.create(user=self.user, complete=True, active=False)
        self.client.force_login(self.user)

    def test_del_confirmation_uses_correct_template(self):
        response = self.client.get('/lists/{}/del/confirm'.format(self.the_list.slug))
        self.assertTemplateUsed(response, 'lists/confirm_delete.html')

    def test_del_confirmation_passes_user_to_context_if_in_url_param(self):
        response = self.client.get('/builds/{}/u/{}/del/confirm'.format(self.the_build.slug, self.user))
        self.assertEqual(response.context['user'], self.user)

    def test_del_list(self):
        response = self.client.post('/lists/{}/del'.format(self.the_list.slug))
        self.assertRedirects(response, '/lists/')

    def test_del_profile_list(self):
        response = self.client.post('/users/{}/lists/{}/del'.format(self.user, self.the_list.slug))
        self.assertRedirects(response, '/users/{}/lists/'.format(self.user))

    def test_del_profile_build(self):
        response = self.client.post('/users/{}/builds/{}/del'.format(self.user, self.the_build.slug))
        self.assertRedirects(response, '/users/{}/builds/'.format(self.user))

    def test_cannot_del_other_users_list(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/lists/{}/del'.format(self.the_list.slug))

        self.assertEqual(response.status_code, 403)

    def test_405_if_not_POST(self):
        response = self.client.get('/lists/{}/del'.format(self.the_list.slug))
        self.assertEqual(response.status_code, 405)


class OpenListTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.the_list = ItemList.objects.create(user=self.user)
        self.the_build = ItemList.objects.create(user=self.user, complete=True, active=False)
        self.client.force_login(self.user)

    def test_mark_list_as_active(self):
        new_list = ItemList.objects.create(user=self.user, active=False)
        self.client.post('/lists/{}/open'.format(new_list.slug))

        new_list = ItemList.objects.get(slug=new_list.slug)
        old_list = ItemList.objects.get(slug=self.the_list.slug)

        self.assertTrue(new_list.active)
        self.assertFalse(old_list.active)

    def test_mark_active_list_if_no_current_active_list(self):
        self.the_list.active = False
        self.the_list.save()
        new_list = ItemList.objects.create(user=self.user, active=False)

        self.client.post('/lists/{}/open'.format(new_list.slug))

        new_list = ItemList.objects.get(slug=new_list.slug)
        old_list = ItemList.objects.get(slug=self.the_list.slug)

        self.assertTrue(new_list.active)
        self.assertFalse(old_list.active)

    def test_mark_list_as_active_redirects_to_list_manage_view(self):
        new_list = ItemList.objects.create(user=self.user, active=False)
        response = self.client.post('/lists/{}/open'.format(new_list.slug))

        self.assertRedirects(response, '/lists/')

    def test_cannot_mark_other_users_list_as_active(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/lists/{}/open'.format(self.the_list.slug))
        self.assertEqual(response.status_code, 403)

    def test_405_if_not_POST(self):
        response = self.client.get('/lists/{}/open'.format(self.the_list.slug))
        self.assertEqual(response.status_code, 405)


class AddItemTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.the_list = ItemList.objects.create(user=self.user, total_price=11)
        self.part = Part.objects.create(manufacturer='testco', name='test part')
        self.price = PriceBase.objects.create(part=self.part, vendor='PSA', price=11, url='http://example.com')
        self.item = Item.objects.create(itemlist=self.the_list, part=self.part)
        self.client.force_login(self.user)

    def test_add_new_items_to_existing_list(self):
        self.client.post('/lists/add_item/{}'.format(self.part.id))
        new_item = Item.objects.last()
        self.assertIsNot(new_item, self.item)
        self.assertIn(new_item, self.the_list.item_set.all())

    def test_add_item_to_existing_list_with_ajax(self):
        self.client.post('/lists/add_item/{}'.format(self.part.id),
                         content_type='application/json',
                         HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        new_item = Item.objects.last()
        self.assertIsNot(new_item, self.item)
        self.assertIn(new_item, self.the_list.item_set.all())

    def test_add_item_redirects_to_list_manage(self):
        response = self.client.post('/lists/add_item/{}'.format(self.part.id))
        self.assertRedirects(response, '/lists/')

    def test_add_item_ajax_redirects_to_list_manage(self):
        response = self.client.post('/lists/add_item/{}'.format(self.part.id),
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        json_redirect = json.loads((response.content.decode('utf8')))
        self.assertEqual(json_redirect, {'redirectTo': '/lists/'})

    def add_item_increments_list_total_price(self):
        self.client.post('/lists/add_item/{}'.format(self.part.id))
        self.assertEqual(self.the_list.total_price, 22)

    def add_item_does_not_create_new_item_when_list_max_price_exceeded(self):
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()
        self.client.post('/lists/add_item/{}'.format(self.part.id))
        self.assertTrue(self.the_list.item_set.count() != 2)

    def test_add_item_exceed_list_max_price_returns_error_message(self):
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()

        response = self.client.post('/lists/add_item/{}'.format(self.part.id), follow=True)

        error = str(list(response.context['messages'])[0])
        error = ' '.join(error.split())
        self.assertEqual(error, 'Maximum total price exceeded. Please remove some items from your list.')

    def test_add_item_redirects_to_login_if_user_is_not_authenticated(self):
        self.client.logout()
        response = self.client.post('/lists/add_item/{}'.format(self.part.id))
        self.assertRedirects(response, '/accounts/login/')

    def test_cannot_add_item_to_other_users_list(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        self.client.post('/lists/add_item/{}'.format(self.part.id))

        item = Item.objects.last()
        self.assertNotIn(item, self.the_list.item_set.all())

    def test_cannot_add_item_to_other_users_list_with_ajax(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/lists/add_item/{}'.format(self.part.id),
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        item = Item.objects.last()
        self.assertNotIn(item, self.the_list.item_set.all())

    def test_add_item_405_if_not_POST(self):
        response = self.client.get('/lists/add_item/{}'.format(self.part.id))
        self.assertEqual(response.status_code, 405)


class DelItemTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.the_list = ItemList.objects.create(user=self.user, total_price=11)
        self.part = Part.objects.create(manufacturer='testco', name='test part')
        self.price = PriceBase.objects.create(part=self.part, vendor='PSA', price=11, url='http://example.com')
        self.item = Item.objects.create(itemlist=self.the_list, part=self.part)
        self.client.force_login(self.user)

    def test_del_items_from_list(self):
        self.client.post('/lists/{}/i/{}/del'.format(self.the_list.slug, self.item.id))
        self.assertNotIn(self.item, self.the_list.item_set.all())

    def test_del_items_view_redirects_to_list_manage(self):
        response = self.client.post('/lists/{}/i/{}/del'.format(self.the_list.slug, self.item.id))
        self.assertRedirects(response, '/lists/')

    def test_del_item_decrements_list_total_price(self):
        self.the_list.total_price = 14
        self.the_list.save()

        response = self.client.post('/lists/{}/i/{}/del'.format(self.the_list.slug, self.item.id))

        price = ItemList.objects.first().total_price
        self.assertEqual(price, 3)

    def test_del_item_decrements_list_total_price_by_custom_price(self):
        self.the_list.total_price = 14
        self.the_list.save()
        self.item.custom_price = 9
        self.item.save()

        response = self.client.post('/lists/{}/i/{}/del'.format(self.the_list.slug, self.item.id))

        price = ItemList.objects.first().total_price
        self.assertEqual(price, 5)

    def test_cannot_delete_item_from_other_users_list(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/lists/{}/i/{}/del'.format(self.the_list.slug, self.item.id))

        self.assertEqual(response.status_code, 403)

    def test_del_items_405_if_not_POST(self):
        response = self.client.get('/lists/{}/del'.format(self.the_list.slug, self.item.id))
        self.assertEqual(response.status_code, 405)


class EditItemTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.the_list = ItemList.objects.create(user=self.user, total_price=11)
        self.part = Part.objects.create(manufacturer='testco', name='test part')
        self.price = PriceBase.objects.create(part=self.part, vendor='PSA', price=11, url='http://example.com')
        self.item = Item.objects.create(itemlist=self.the_list, part=self.part)
        self.client.force_login(self.user)

    def test_edit_item_uses_edit_item_form(self):
        response = self.client.get('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id))
        self.assertIsInstance(response.context['form'], EditItemForm)

    def test_choose_vendor_of_existing_item_in_list(self):
        PriceBase.objects.create(part=self.part, vendor='MUSA', price=199)
        self.client.post('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id),
                         data={'vendor': 'MUSA', 'choose_vendor': 'True'})
        item = Item.objects.get(part=self.part)
        self.assertEqual(item.vendor, 'MUSA')
        self.assertEqual(item.price, 199)

    def test_edit_custom_price_of_existing_item_in_list(self):
        self.client.post('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id),
                         data={'custom_price': 200, 'choose_price': 'True'})

        item = Item.objects.get(part=self.part)
        self.assertEqual(item.custom_price, 200)

    def test_edit_custom_price_changes_list_total_price(self):
        self.client.post('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id),
                         data={'custom_price': 200, 'choose_price': 'True'})

        new_price = ItemList.objects.first().total_price
        self.assertEqual(new_price, 200)

    def test_edit_existing_custom_price_changes_list_total_price(self):
        self.item.custom_price = 100
        self.item.save()
        self.the_list.total_price = 100
        self.the_list.save()

        self.client.post('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id),
                         data={'custom_price': 200, 'choose_price': 'True'})

        new_price = ItemList.objects.first().total_price
        self.assertEqual(new_price, 200)

    def test_select_new_vendor_increments_list_total_price(self):
        PriceBase.objects.create(part=self.part, vendor='MUSA', price=199)
        self.client.post('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id),
                         data={'vendor': 'MUSA', 'choose_vendor': 'True'})

        new_price = ItemList.objects.first().total_price
        self.assertEqual(new_price, 199)

    def test_select_new_vendor_increments_list_total_price_with_existing_custom_price(self):
        self.item.custom_price = 211
        self.item.save()
        self.the_list.total_price = 211
        self.the_list.save()

        PriceBase.objects.create(part=self.part, vendor='MUSA', price=199)
        self.client.post('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id),
                         data={'vendor': 'MUSA', 'choose_vendor': 'True'})

        new_price = ItemList.objects.first().total_price
        self.assertEqual(new_price, 199)

    def test_edit_item_redirects_to_list_manage(self):
        response = self.client.post('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id),
                                    data={'custom_price': 200, 'choose_price': 'True'})

        self.assertRedirects(response, '/lists/')

    def test_do_not_edit_item_if_exceed_list_max_price(self):
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()

        response = self.client.post('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id),
                                    data={'custom_price': 9999, 'choose_price': 'True'})

        custom_price = Item.objects.get(id=self.item.id).custom_price

        self.assertNotEqual(custom_price, 9999)

    def test_edit_item_exceed_list_max_produces_error_message(self):
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()

        response = self.client.post('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id),
                                    follow=True,
                                    data={'custom_price': 9999, 'choose_price': 'True'})

        error = str(list(response.context['messages'])[0])
        error = ' '.join(error.split())
        self.assertEqual(error, 'Maximum total price exceeded. Please remove some items from your list.')

    def test_cannot_edit_items_from_other_users_list(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/lists/{}/i/{}/edit'.format(self.the_list.slug, self.item.id),
                                    data={'price': 50.00})

        self.assertEqual(response.status_code, 403)


class CustomItemTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.the_list = ItemList.objects.create(user=self.user)
        self.client.force_login(self.user)

    def test_add_custom_item_uses_custom_item_form(self):
        response = self.client.get('/lists/add_custom')
        self.assertIsInstance(response.context['form'], CustomItemForm)

    def test_add_custom_item_to_list(self):
        self.client.post('/lists/add_custom'.format(self.the_list.slug),
                         data={'category': 'other',
                               'part': 'custom part',
                               'price': 100,
                               'url': 'http://part.com'})

        custom = self.the_list.customitem_set.all()
        self.assertTrue(custom.exists())

    def test_do_not_add_custom_item_if_list_max_price_exceeded(self):
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()

        response = self.client.post('/lists/add_custom'.format(self.the_list.slug),
                                    data={'category': 'other',
                                          'part': 'custom part',
                                          'price': 100,
                                          'url': 'http://part.com'})

        self.assertFalse(CustomItem.objects.all())

    def test_add_custom_item_if_list_max_price_exceeded_returns_error_message(self):
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()

        response = self.client.post('/lists/add_custom'.format(self.the_list.slug),
                                    follow=True,
                                    data={'category': 'other',
                                          'part': 'custom part',
                                          'price': 100,
                                          'url': 'http://part.com'})

        error = str(list(response.context['messages'])[0])
        error = ' '.join(error.split())
        self.assertEqual(error, 'Maximum total price exceeded. Please remove some items from your list.')

    def test_add_custom_redirects_list_manage(self):
        response = self.client.post('/lists/add_custom'.format(self.the_list.slug),
                                    data={'category': 'other',
                                          'part': 'custom part',
                                          'price': 100,
                                          'url': 'http://part.com'})

        self.assertRedirects(response, '/lists/')

    def test_cannot_add_custom_item_to_other_users_list(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/lists/add_custom'.format(self.the_list.slug),
                                    data={'part': 'custom part',
                                          'price': 100,
                                          'url': 'http://part.com'})

        self.assertNotIn(CustomItem.objects.first(), self.the_list.customitem_set.all())

    def test_del_custom_item_from_list(self):
        custom_item = CustomItem.objects.create(itemlist=self.the_list, part='a part', price=5.55)

        self.client.post('/lists/{}/c/{}/del'.format(self.the_list.slug, custom_item.id))

        deleted_custom = self.the_list.customitem_set.all()
        self.assertFalse(deleted_custom.exists())

    def test_cannot_del_custom_item_from_other_users_list(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        custom_item = CustomItem.objects.create(itemlist=self.the_list, part='a part', price=5.55)

        response = self.client.post('/lists/{}/c/{}/del'.format(self.the_list.slug, custom_item.id))

        self.assertEqual(response.status_code, 403)

    def test_405_if_del_custom_item_not_POST(self):
        custom_item = CustomItem.objects.create(itemlist=self.the_list, part='a part', price=5.55)
        response = self.client.get('/lists/{}/c/{}/del'.format(self.the_list.slug, custom_item.id))
        self.assertEqual(response.status_code, 405)

    def test_edit_custom_item_view_uses_custom_item_form(self):
        custom = CustomItem.objects.create(itemlist=self.the_list, category='trigger', part='custom part', price=100)
        response = self.client.get('/lists/{}/c/{}/edit'.format(self.the_list.slug, custom.id))
        self.assertIsInstance(response.context['form'], CustomItemForm)

    def test_edit_custom_item(self):
        custom = CustomItem.objects.create(itemlist=self.the_list, category='trigger', part='custom part', price=100)
        self.client.post('/lists/{}/c/{}/edit'.format(self.the_list.slug, custom.id),
                         data={'part': 'edit custom',
                               'price': 200,
                               'category': 'stock'})

        custom = CustomItem.objects.get(id=custom.id)
        self.assertEqual(custom.category, 'stock')

    def test_edit_custom_item_price_does_not_exceed_list_max_price(self):
        custom = CustomItem.objects.create(itemlist=self.the_list, category='trigger', part='custom part', price=100)
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()

        self.client.post('/lists/{}/c/{}/edit'.format(self.the_list.slug, custom.id),
                         data={'part': 'edit custom',
                               'price': 200,
                               'category': 'stock'})

        custom = CustomItem.objects.get(id=custom.id)
        self.assertNotEqual(custom.price, 200)

    def test_edit_custom_item_exceeds_max_price_returns_error_message(self):
        custom = CustomItem.objects.create(itemlist=self.the_list, category='trigger', part='custom part', price=100)
        self.the_list.total_price = LIST_MAX_PRICE
        self.the_list.save()

        response = self.client.post('/lists/{}/c/{}/edit'.format(self.the_list.slug, custom.id),
                                    follow=True,
                                    data={'part': 'edit custom',
                                          'price': 200,
                                          'category': 'stock'})

        error = str(list(response.context['messages'])[0])
        error = ' '.join(error.split())

        self.assertEqual(error, 'Maximum total price exceeded. Please remove some items from your list.')


class CompleteListTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.the_list = ItemList.objects.create(user=self.user, complete=True, active=False)
        self.part = Part.objects.create(manufacturer='a', product_num='slug', name='gat part')
        self.item = Item.objects.create(itemlist=self.the_list, part=self.part)
        self.client.force_login(self.user)

    def test_marking_list_as_complete_redirects_to_build_edit_view(self):
        complete_list = ItemList.objects.create(user=self.user)
        item = Item.objects.create(itemlist=complete_list, part=self.part)
        response = self.client.post('/builds/{}/complete'.format(complete_list.slug))

        self.assertRedirects(response, '/builds/{}/edit'.format(complete_list.slug))

    def test_must_have_at_least_one_item_to_mark_list_as_complete(self):
        empty_list = ItemList.objects.create(user=self.user)
        self.client.post('/builds/{}/complete'.format(empty_list.slug))

        empty_list = ItemList.objects.get(slug=empty_list.slug)
        self.assertFalse(empty_list.complete)

    def test_complete_list_view_returns_error_message_if_empty_list(self):
        empty_list = ItemList.objects.create(user=self.user)
        response = self.client.post('/builds/{}/complete'.format(empty_list.slug), follow=True)

        error = str(list(response.context['messages'])[0])
        error = ' '.join(error.split())

        self.assertEqual(error, 'You must add at least one item or custom item to your list before marking it as complete.')

    def test_complete_list_view_redirects_to_list_manage_view_if_error(self):
        empty_list = ItemList.objects.create(user=self.user)
        response = self.client.post('/builds/{}/complete'.format(empty_list.slug), follow=True)

        self.assertRedirects(response, '/lists/')

    def test_cannot_complete_other_users_lists(self):
        self.the_list.complete = False
        self.the_list.save()
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/builds/{}/complete'.format(self.the_list.slug))

        self.assertEqual(response.status_code, 403)

    def test_edit_complete_build_items(self):
        self.client.post('/builds/{}/list'.format(self.the_list.slug))

        complete = ItemList.objects.get(id=self.the_list.id).complete
        self.assertFalse(complete)

    def test_edit_complete_build_items_sets_currently_active_list_to_inactive(self):
        active_list = ItemList.objects.create(user=self.user, active=True)
        self.client.post('/builds/{}/list'.format(self.the_list.slug))

        active_list = ItemList.objects.get(id=active_list.id)
        self.assertFalse(active_list.active)

    def test_edit_complete_build_items_redirects_to_list_manage_view(self):
        response = self.client.post('/builds/{}/list'.format(self.the_list.slug))
        self.assertRedirects(response, '/lists/')

    def test_cannot_edit_other_users_complete_build_items(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/builds/{}/list'.format(self.the_list.slug))

        self.assertEqual(response.status_code, 403)

    def test_display_completed_lists(self):
        response = self.client.get('/builds/')
        self.assertIn(self.the_list, response.context['builds'])

    def test_completed_build_uses_correct_template(self):
        response = self.client.get('/builds/{}/'.format(self.the_list.slug))
        self.assertTemplateUsed(response, template_name='lists/build.html')

    def test_edit_complete_list_description_uses_edit_build_form(self):
        response = self.client.get('/builds/{}/edit'.format(self.the_list.slug))
        self.assertIsInstance(response.context['form'], EditBuildForm)

    def test_edit_complete_list(self):
        self.client.post('/builds/{}/edit'.format(self.the_list.slug),
                         data={'title': 'A New Title',
                               'description': 'I did it',
                               'deck': 'yo'})

        the_list = ItemList.objects.first()

        self.assertEqual(the_list.title, 'A New Title')
        self.assertEqual(the_list.description, 'I did it')

    def test_cannot_edit_other_users_list(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/builds/{}/edit'.format(self.the_list.slug),
                                    data={'title': 'A New Title',
                                          'description': 'I did it'})

        self.assertEqual(response.status_code, 403)

    def test_rating_part_uses_part_rating_form(self):
        response = self.client.get('/builds/{}/reviews/i/{}/edit'.format(self.the_list.slug, self.item.id))
        self.assertIsInstance(response.context['form'], PartReviewForm)

    def test_rating_part_from_complete_list(self):
        self.client.post('/builds/{}/reviews/i/{}/edit'.format(self.the_list.slug, self.item.id),
                         data={'rating': 5,
                               'review': 'cool'})

        rating = self.part.partreview_set.first()
        self.assertEqual(rating.rating, 5)
        self.assertEqual(rating.review, 'cool')

    def test_can_edit_part_rating(self):
        self.client.post('/builds/{}/reviews/i/{}/edit'.format(self.the_list.slug, self.item.id),
                         data={'rating': 5,
                               'review': 'cool'})

        old_rating = self.part.partreview_set.first().rating
        old_review = self.part.partreview_set.first().review

        self.client.post('/builds/{}/reviews/i/{}/edit'.format(self.the_list.slug, self.item.id),
                         data={'rating': 1,
                               'review': 'trash'})

        new_rating = self.part.partreview_set.first().rating
        new_review = self.part.partreview_set.first().review

        self.assertNotEqual(old_rating, new_rating)
        self.assertNotEqual(old_review, new_review)

    def test_cannot_rate_other_users_items(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/builds/{}/reviews/i/{}/edit'.format(self.the_list.slug, self.item.id),
                                    data={'rating': 5,
                                          'review': 'cool'})

        self.assertEqual(response.status_code, 403)

    def test_delete_part_rating(self):
        review = PartReview.objects.create(user=self.user, build=self.the_list, part=self.part, item=self.item, rating=5, review='good')
        self.client.post('/builds/{}/reviews/r/{}/del'.format(self.the_list.slug, review.id))

        self.assertFalse(PartReview.objects.all())

    def test_cannot_delete_other_users_part_rating(self):
        review = PartReview.objects.create(user=self.user, build=self.the_list, part=self.part, item=self.item,
                                           rating=5, review='good')
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/builds/{}/reviews/r/{}/del'.format(self.the_list.slug, review.id))

        self.assertEqual(response.status_code, 403)

    def test_405_if_delete_part_rating_not_POST(self):
        review = PartReview.objects.create(user=self.user, build=self.the_list, part=self.part, item=self.item,
                                           rating=5, review='good')
        response = self.client.get('/builds/{}/reviews/r/{}/del'.format(self.the_list.slug, review.id))
        self.assertEqual(response.status_code, 405)


class ExportListTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.the_list = ItemList.objects.create(user=self.user)
        self.part = Part.objects.create(manufacturer='PartCo', name='Test Part', category='trigger')
        self.price = PriceBase.objects.create(part=self.part, vendor='PSA', price=3.50)
        self.item = Item.objects.create(part=self.part, itemlist=self.the_list)
        self.custom = CustomItem.objects.create(category='stock', part='Custom Part', itemlist=self.the_list, price=11)
        self.maxDiff = None

    def test_export_list_markdown(self):
        response = self.client.get('/lists/{}/export/markdown'.format(self.the_list.slug),
                                   content_type='application/json',
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest'
                                   )
        data = response.content.decode('utf-8')
        data = json.loads(data)
        export_list = {'table': '### [](http://example.com/lists/{}/)\nCategory|Item|Price\n:---|:---|---:\n**Stock** | Custom Part | $11.00 → Custom Item\n**Trigger** | [Test Part](http://example.com/parts/{}/test-part/) | $3.50 → Palmetto State Armory\n| | **Total** | **$0.00**\n| | [*Created @ Gatpicker.com*](http://example.com) |'.format(self.the_list.slug, self.part.slug)}
        self.assertEqual(data, export_list)

    def test_export_list_bbcode(self):
        response = self.client.get('/lists/{}/export/bbcode'.format(self.the_list.slug),
                                   content_type='application/json',
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest'
                                   )
        data = response.content.decode('utf-8')
        data = json.loads(data)
        export_list = {'table': '[url=http://example.com/lists/{}/][/url]\n\n[b]Stock[/b]: Custom Part - [$11.00 → Custom Item]\n[b]Trigger:[/b] [url=http://example.com/parts/{}/test-part/]Test Part[/url] - [$3.50 → Palmetto State Armory]\n[b]Total[/b]: $0.00\n\n[url=http://example.com]Created @ Gatpicker.com[/url]'.format(self.the_list.slug, self.part.slug)}
        self.assertEqual(data, export_list)

    def test_export_list_html(self):
        response = self.client.get('/lists/{}/export/html'.format(self.the_list.slug),
                                   content_type='application/json',
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest'
                                   )
        data = response.content.decode('utf-8')
        data = json.loads(data)
        export_list = {'table': '<a href="http://example.com/lists/{}/"></a>\n<br>\n<table>\n    <thead>\n        <tr>\n            <th>Category</th>\n            <th>Item</th>\n            <th>Price</th>\n        </tr>\n    </thead>\n    <tbody>\n        <tr>\n            <td><b>Stock</b></td>\n            <td>Custom Part</td>\n            <td>$11.00 → Custom Item</td>\n        </tr>\n        <tr>\n            <td><b>Trigger</b></td>\n            <td><a href=\'http://example.com/parts/{}/test-part/\'>Test Part</a></td>\n            <td>$3.50 → Palmetto State Armory</td>\n        </tr>\n        <tr>\n            <td></td>\n            <td><b>Total:</b> </td>\n            <td>$0.00</td>\n        </tr>\n        <tr>\n            <td></td>\n            <td><a href="=http://example.com">Created @ Gatpicker.com</a></td>\n            <td></td>\n        </tr>\n    </tbody>\n</table>'.format(self.the_list.slug, self.part.slug)}
        self.assertEqual(data, export_list)

    def test_export_list_text(self):
        response = self.client.get('/lists/{}/export/text'.format(self.the_list.slug),
                                   content_type='application/json',
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest'
                                   )
        data = response.content.decode('utf-8')
        data = json.loads(data)
        export_list = {'table': ': http://example.com/lists/{}/\n\nStock: Custom Part - [$11.00 → Custom Item]\nTrigger: Test Part - [$3.50 → Palmetto State Armory]\n\nTotal: $0.00\nCreated @ Gatpicker.com'.format(self.the_list.slug)}
        self.assertEqual(data, export_list)
