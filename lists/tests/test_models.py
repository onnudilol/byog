from django.test import TestCase
from django.contrib.auth.models import User

from lists.models import ItemList, Item, BuildImage
from parts.models import Part, PriceBase


class ListTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='marfalo')
        self.the_list = ItemList.objects.create(user=self.user)

    def test_slug_generated_on_list_creation(self):
        new_slug = ItemList.objects.create(user=self.user)
        self.assertIsNotNone(new_slug.slug)

    def test_slug_doesnt_change_when_list_saved(self):
        old_slug = self.the_list.slug
        self.the_list.save()
        new_slug = self.the_list.slug

        self.assertEqual(old_slug, new_slug)

    def test_get_absolute_url(self):
        self.assertEqual(self.the_list.get_absolute_url(), '/lists/{}/'.format(self.the_list.slug))

    def test_get_absolute_url_complete_build(self):
        build = ItemList.objects.create(user=self.user, complete=True)
        self.assertEqual(build.get_absolute_url(), '/builds/{}/'.format(build.slug))


class ItemTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='marfalo')
        self.the_list = ItemList.objects.create(user=self.user)
        self.part = Part.objects.create(category='trigger', manufacturer='test', name='test tester', product_num='+35+')
        self.price = PriceBase.objects.create(part=self.part, vendor='PSA', price=11)

    def test_item_inherits_item_properties_on_creation(self):
        item = Item.objects.create(itemlist=self.the_list, part=self.part)
        self.assertEqual(item.vendor, 'PSA')
        self.assertEqual(item.price, 11)
        self.assertEqual(item.category, 'trigger')

    def test_item_vendor_and_category_do_not_change_if_already_set(self):
        item = Item.objects.create(itemlist=self.the_list, part=self.part, category='stock', price=25, vendor='MUSA')
        item.save()
        self.assertNotEqual(item.vendor, 'PSA')
        self.assertNotEqual(item.price, 11)
        self.assertNotEqual(item.category, 'trigger')


class BuildImageTest(TestCase):

    def test_build_image_save_sets_user(self):
        user = User.objects.create(username='marfalo')
        build = ItemList.objects.create(user=user, complete=True)
        img = BuildImage.objects.create(build=build)

        self.assertEqual(img.user, user)
