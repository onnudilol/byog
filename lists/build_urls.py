from django.conf.urls import url

from lists import views

urlpatterns = [
    url(r'^$', views.BuildList.as_view(), name='build_list'),
    url(r'^featured/$', views.BuildListFeatured.as_view(), name='build_featured'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/u/(?P<username>[\w._@+-]{1,30})/edit/confirm$', views.BuildEditConfirmation.as_view(), name='build_confirm_edit'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/u/(?P<username>[\w._@+-]{1,30})/del/confirm$', views.ListDelConfirmation.as_view(), name='build_confirm_del'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/$', views.build_manage, name='build_manage'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/vote$', views.build_vote, name='build_vote'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/complete$', views.complete_build, name='build_complete'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/edit$', views.edit_build, name='build_edit'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/list$', views.edit_build_list, name='build_list_edit'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/image/$', views.build_image, name='build_image'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/reviews/$', views.build_review, name='build_review'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/reviews/i/(?P<item_id>[0-9]+)/edit$', views.edit_review, name='build_review_edit'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/reviews/r/(?P<review_id>[0-9]+)/del$', views.del_review, name='build_review_del'),
    url(r'^img/(?P<image_id>[0-9]+)/del$', views.del_build_image, name='build_image_del'),
]
