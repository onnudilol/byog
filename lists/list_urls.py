from django.conf.urls import url

from lists import views

urlpatterns = [
    url(r'^$', views.list_manage, name='list_manage'),
    url(r'^add_custom', views.add_custom_item, name='custom_add'),
    url(r'^add_item/(?P<part_id>[0-9]+)$', views.add_item, name='item_add'),
    url(r'^new$', views.new_list, name='list_new'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/del/confirm$', views.ListDelConfirmation.as_view(), name='list_confirm_del'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/$', views.list_manage, name='list_public'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/del$', views.del_list, name='list_del'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/edit', views.edit_list, name='list_edit'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/open', views.open_list, name='list_open'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/export/(?P<format_type>[A-Za-z]+)', views.export_list, name='list_export'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/i/(?P<item_id>[0-9]+)/edit$', views.edit_item, name='item_edit'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/i/(?P<item_id>[0-9]+)/del$', views.del_item, name='item_del'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/c/(?P<item_id>[0-9]+)/edit$', views.edit_custom_item, name='custom_edit'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/c/(?P<item_id>[0-9]+)/del$', views.del_custom_item, name='custom_del'),
]
