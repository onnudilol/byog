$(document).ready(function () {
    var check = $('#tos-check:checkbox');
    var tos = $('#disable-input');

    if (check.is(':checked')) {
        if (tos.attr('disabled')) {
            tos.removeAttr('disabled');
        }
    }
    else {
        tos.attr('disabled', true);
    }

    check.change(function () {
        if (check.is(':checked')) {
            tos.removeAttr('disabled');
        }
        else {
            tos.attr('disabled', true);
        }
    });
});