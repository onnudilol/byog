;function starFilter(rating, total) {
    var star_glyph = '<i class="fa fa-star" aria-hidden="true"></i>';
    var empty_star_glyph = '<i class="fa fa-star-o" aria-hidden="true"></i>';
    var half_star_glyph = '<i class="fa fa-star-half-o" aria-hidden="true"></i>';
    var stars = null;

    format.extend(String.prototype, {});

    // No ratings, return five empty stars
    if (rating.startsWith('None')) {
        stars = '{} ({})'.format(empty_star_glyph.repeat(5), total);
        return stars
    }

    else {
        // Retrieve the integer and decimal parts of the rating float
        // Actual value of the decimal part is irrelevant; all fractions are represented by half stars
        var int_part = Math.floor(rating);
        var float_part = rating - int_part;
        var empty_part = 5 - int_part
    }

    // create and return the star string
    if (float_part > 0) {
        stars = '{} ({})'.format((star_glyph.repeat(int_part - 1) + empty_star_glyph.repeat(empty_part) + half_star_glyph), total);
    }
    else {
        stars = '{} ({})'.format((star_glyph.repeat(int_part) + empty_star_glyph.repeat(empty_part)), total);
    }

    return stars;
}