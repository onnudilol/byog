;function initDT(categoryURL) {

    var table = $('#data-table').DataTable({
        columnDefs: [
            {
                orderable: false,
                targets: [-1]
            }
        ],
        aaSorting: [],
        pageLength: 25,
        processing: true,
        paging: true,
        serverSide: true,
        ajax: {
            url: categoryURL,
            type: "POST",
            data: function(data) {
                var filters = {};
                var sidebar = $('#filter-sidebar');

                // creates price min/max filters
                var priceSlider = $('#price-slider').val().split(',');
                var priceMin = priceSlider[0];
                var priceMax = priceSlider[1];
                filters['prices'] = [priceMin, priceMax];

                // creates empty entries in filters
                sidebar.children('.filter-list').each(function() {
                   filters[$(this).data('spec')] = [];
                });

                // for each checked box in the filter
                sidebar.find('input:checked').each(function() {
                    var checked = $(this).val();
                    var checked_key =  $(this).parents('div.filter-list').data('spec');

                    // don't add key to filter array if already present
                    if (filters[checked_key].indexOf(checked) === -1) {
                        filters[checked_key].push(checked);
                    }
                });
                data.filters = filters
            }
        }

    });

    function recolourButton() {
        $(" li.paginate_button > a ").addClass("pagination_button");

        $('span.star-rating').each(function () {
            $(this).replaceWith(starFilter($(this).data("rating"), $(this).data("total")));
        });

        $("a.category-add-button").each(function () {
            $(this).parent("td").attr({align: 'center'})
        });
    }

    table.on('draw.dt', function () {
        recolourButton();
    });

    table.on('search.dt', function() {
        recolourButton();
    });

    $(" li.paginate_button > a ").addClass("pagination_button");

    $('#filter-button').click(function() {
        table.ajax.reload();
    });

    $('#clear-filters').click(function() {
        $('#filter-sidebar').find('input:checked').each(function() {
            $(this).prop('checked', false);
        });
        var priceSlider = $('#price-slider');
        priceSlider.slider('refresh');
        $("#price-min").text('$' + priceSlider.data('slider-min'));
        $("#price-max").text('$' + priceSlider.data('slider-max'));
        table.ajax.reload();
    });

    return table;
}