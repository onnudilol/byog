;function exportList(format, slug) {
    $.get({
        url: '/lists/' + slug + '/export/' + format,
        data: {'csrftoken': csrftoken}
    })
        .done(function (data) {
            $('#export-text').val(data['table']);
            $(".modal").on('shown.bs.modal', function () {
                $(this).find("#export-text").focus().select();
            });
        });
}