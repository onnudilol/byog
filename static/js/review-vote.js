;function ReviewVote () {
    $('.review-helpful').click(function () {
        $(this).removeClass('btn-default').addClass('review-btn-success');
        $(this).parents('.review-controls').find('.review-unhelpful').removeClass('review-btn-danger').addClass('btn-default');
    });

    $('.review-unhelpful').click(function () {
        $(this).removeClass('btn-default').addClass('review-btn-danger');
        $(this).parents('.review-controls').find('.review-helpful').removeClass('review-btn-success').addClass('btn-default');
    });

    $('article').readmore();
}