;function VoteReview(review_id, helpful) {
    $.post({
        url: "/parts/review/" + review_id + "/rate",
        data: {"helpful": helpful, "csrftoken": csrftoken}
    });
}

;function VoteBuild(slug) {
    $.post({
        url: "/builds/" + slug + "/vote",
        data: {"csrftoken": csrftoken}
    })
        .done(function(data) {
            var btn = $("#build-vote-btn");
            btn.toggleClass('btn-primary').toggleClass('review-btn-success');

            var scoreSpan = $('#build-score');
            var score = parseInt(scoreSpan.text());

            if (data['created'] === false) {
                score -= 1
            }
            else {
                score += 1
            }

            scoreSpan.text(score);
        });
}
