$('#navbar-category').click(function (event) {
    var toggle_bool = $('#navbar-toggle').attr('aria-expanded');
    if (toggle_bool === 'false') {
        event.preventDefault();
        $('#category-dropdown').slideToggle();
    }
});