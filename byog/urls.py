"""byog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from common.views import HomePageView, TOSView
from parts import urls as parts_urls
from lists import list_urls, build_urls
from profiles import urls as profile_urls
from articles import urls as article_urls

urlpatterns = [
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^tos/$', TOSView.as_view(), name='tos'),
    url(r'^operator/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^parts/', include(parts_urls)),
    url(r'^lists/', include(list_urls)),
    url(r'^builds/', include(build_urls)),
    url(r'^users/', include(profile_urls)),
    url(r'^article/', include(article_urls)),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
