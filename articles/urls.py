from django.conf.urls import url
from articles import views

urlpatterns = [
    url(r'^$', views.ArticleList.as_view(), name='article_list'),
    url(r'^(?P<slug>[a-z0-9_-]+)/$', views.article_detail, name='article_detail'),
]
