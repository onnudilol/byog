from django.contrib import admin

from articles.models import Article, ArticleCoverImage


class ArticleCoverImageInline(admin.TabularInline):
    model = ArticleCoverImage


class ArticleAdmin(admin.ModelAdmin):
    model = Article
    inlines = [ArticleCoverImageInline]
    raw_id_fields = ['author']
    readonly_fields = ['slug']
    list_display = ['title', 'author', 'date']

admin.site.register(Article, ArticleAdmin)
