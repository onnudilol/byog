from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.dispatch import receiver
from django.core.urlresolvers import reverse

from ckeditor_uploader.fields import RichTextUploadingField
from taggit.managers import TaggableManager
from sorl.thumbnail import ImageField

import os
import uuid


# From http://stackoverflow.com/questions/2673647/enforce-unique-upload-file-names-using-django
def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('img/articles/', filename)


class Article(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=500, unique=True)
    deck = models.CharField(max_length=140)
    slug = models.CharField(max_length=500, unique=True)
    date = models.DateTimeField(auto_now_add=True)
    content = RichTextUploadingField()

    tags = TaggableManager()

    class Meta:
        ordering = ['-date']

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)

        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('article_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title


class ArticleCoverImage(models.Model):
    image = ImageField(upload_to=get_file_path, blank=True)
    article = models.OneToOneField('articles.Article', on_delete=models.CASCADE)


# From http://stackoverflow.com/a/16041527
@receiver(models.signals.post_delete, sender=ArticleCoverImage)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Deletes an uploaded image on disk when the associated model is deleted"""

    if instance.image:

        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


@receiver(models.signals.pre_save, sender=ArticleCoverImage)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem when corresponding ArticleCoverImage object is changed."""

    if not instance.pk:
        return False

    try:
        old_image = ArticleCoverImage.objects.get(pk=instance.pk).image

    except ArticleCoverImage.DoesNotExist:
        return False

    new_image = instance.image

    if old_image:

        if not old_image == new_image:

            if instance.image == old_image:
                instance.image = new_image
                instance.save()

            if os.path.isfile(old_image.path):
                os.remove(old_image.path)
