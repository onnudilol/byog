from django.test import TestCase
from django.contrib.auth import get_user_model

from articles.models import Article
from articles.views import ArticleList


User = get_user_model()


class ArticleListTest(TestCase):

    def setUp(self):
        self.author = User.objects.create(username='marfalo')
        self.author2 = User.objects.create(username='partario')

        self.article1 = Article.objects.create(title='Test Post Please Ignore', author=self.author)
        self.article1.tags.add('test', 'post', 'please ignore')

        self.article2 = Article.objects.create(title='Real Post Please Respond', author=self.author2)
        self.article2.tags.add('real', 'post', 'please respond')

        self.article3 = Article.objects.create(title='shit post', author=self.author2)
        self.article3.tags.add('shitpost', 'trash', 'garbage')

    def test_article_list_returns_all_articles_if_no_query_string(self):
        response = self.client.get('/article/')
        self.assertIn(self.article1, response.context['articles'])
        self.assertIn(self.article2, response.context['articles'])

    def test_article_list_fetches_tagged_articles(self):
        response = self.client.get('/article/?tags=test')
        self.assertIn(self.article1, response.context['articles'])

    def test_article_list_fetches_all_matching_articles(self):
        response = self.client.get('/article/?tags=test,please+respond')
        self.assertIn(self.article1, response.context['articles'])
        self.assertIn(self.article2, response.context['articles'])

    def test_article_fetches_authors_articles(self):
        response = self.client.get('/article/?author=marfalo')
        self.assertIn(self.article1, response.context['articles'])

    def test_article_fetches_both_tags_and_authors(self):
        response = self.client.get('/article/?tags=post&author=partario')
        self.assertIn(self.article2, response.context['articles'])

    def test_article_list_returns_nothing_if_no_params_matched(self):
        response = self.client.get('/article/?tags=nothing&author=nobody')
        self.assertFalse(response.context['articles'])

    def test_OR_clause_within_parameter(self):
        response = self.client.get('/article/?tags=test,real')
        self.assertIn(self.article1, response.context['articles'])
        self.assertIn(self.article2, response.context['articles'])

    def test_AND_clause_between_parameters(self):
        response = self.client.get('/article/?tags=shitpost&author=partario')
        self.assertIn(self.article3, response.context['articles'])
        self.assertNotIn(self.article2, response.context['articles'])

    def test_filter_does_not_return_duplicates(self):
        response = self.client.get('/article/?tags=test,please+ignore')
        self.assertEqual(len(response.context['articles']), 1)


class ArticleDetailView(TestCase):

    def setUp(self):
        self.author = User.objects.create(username='marfalo')
        self.article = Article.objects.create(title='Test Post Please Ignore', author=self.author)
        self.article.tags.add('test', 'post', 'please ignore')

    def test_generate_disqus_code_if_user_is_authenticated(self):
        self.client.force_login(self.author)
        response = self.client.get('/article/{}/'.format(self.article.slug))
        self.assertNotEqual(response.context['disqus'], '')

    def test_does_not_generate_disqus_code_if_user_not_authenticated(self):
        response = self.client.get('/article/{}/'.format(self.article.slug))
        self.assertEqual(response.context['disqus'], '')
