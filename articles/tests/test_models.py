from django.test import TestCase
from django.contrib.auth import get_user_model

from articles.models import Article, ArticleCoverImage

User = get_user_model()


class ArticleTest(TestCase):

    def test_article_saves_title_slug(self):
        author = User.objects.create()
        art = Article.objects.create(title='This Is A Title', author=author)
        self.assertEqual(art.slug, 'this-is-a-title')
