from django.test import TestCase
from django.contrib.auth import get_user_model
from django.core.files import File


from profiles.models import UserProfile
from profiles.forms import UserProfileForm
from lists.models import ItemList
from lists.forms import ListDetailForm

User = get_user_model()


class ProfileTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.client.force_login(self.user)

    def test_profile_view_uses_correct_template(self):
        response = self.client.get('/users/{}/'.format(self.user.username))
        self.assertTemplateUsed(response, 'profiles/profile.html')

    def test_user_profile_view_retrieves_correct_user_profile(self):
        UserProfile.objects.create(user=self.user)
        response = self.client.get('/users/{}/'.format(self.user.username))
        self.assertEqual(self.user.userprofile, response.context['profile'])

    def test_view_does_not_500_when_user_has_no_profile(self):
        response = self.client.get('/users/{}/'.format(self.user.username))
        self.assertEqual(response.status_code, 200)

    def test_view_passes_user_info_to_context(self):
        response = self.client.get('/users/{}/'.format(self.user.username))
        self.assertEqual(response.context['username'], self.user.username)
        self.assertEqual(response.context['date_joined'], self.user.date_joined)

    def test_view_does_not_500_if_user_profile_has_no_images(self):
        UserProfile.objects.create(user=self.user)
        response = self.client.get('/users/{}/'.format(self.user.username))
        self.assertFalse(response.context['user_image'])

    def test_404_for_nonexistent_user(self):
        response = self.client.get('/users/partario/')
        self.assertEqual(response.status_code, 404)


class CreateProfileTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.client.force_login(self.user)

    def test_create_new_profile_view_uses_form(self):
        response = self.client.get('/users/{}/create'.format(self.user.username))
        self.assertIsInstance(response.context['form'], UserProfileForm)

    def test_create_profile_view_uses_correct_form(self):
        response = self.client.get('/users/{}/create'.format(self.user.username))
        self.assertTemplateUsed(response, 'profiles/profile_create_edit.html')

    def test_create_new_profile(self):
        self.client.post('/users/{}/create'.format(self.user.username), data={'description': 'test profile'})
        profile = UserProfile.objects.first()
        self.assertEqual(profile.description, 'test profile')

    def test_create_new_profile_403s_if_user_tries_to_create_other_users_profile(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)
        response = self.client.post('/users/{}/create'.format(self.user.username), data={'description': 'test description'})

        self.assertEqual(response.status_code, 403)

    def test_create_profile_redirects_to_profile_base_view(self):
        response = self.client.post('/users/{}/create'.format(self.user.username), data={'description': 'test description'})
        self.assertRedirects(response, '/users/{}/'.format(self.user.username))


class EditProfileTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.client.force_login(self.user)
        self.profile = UserProfile.objects.create(user=self.user)

    def test_edit_profile_view_uses_form(self):
        response = self.client.get('/users/{}/edit'.format(self.user.username))
        self.assertIsInstance(response.context['form'], UserProfileForm)

    def test_edit_profile(self):
        self.client.post('/users/{}/edit'.format(self.user.username), data={'description': 'test description'})
        profile = UserProfile.objects.first()
        self.assertEqual(profile.description, 'test description')

    def test_edit_profile_redirects_to_profile_base(self):
        response = self.client.post('/users/{}/edit'.format(self.user.username), data={'description': 'test description'})
        self.assertRedirects(response, '/users/{}/'.format(self.user.username))

    def test_other_user_cannot_edit_profile(self):
        self.client.logout()
        user2 = User.objects.create(username='partario')
        self.client.force_login(user2)

        response = self.client.post('/users/{}/edit'.format(self.user.username), data={'description': 'test description'})
        self.assertEqual(response.status_code, 403)


class ProfileListTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.client.force_login(self.user)
        self.list1 = ItemList.objects.create(user=self.user)
        self.list2 = ItemList.objects.create(user=self.user)

    def test_profile_list_404_if_user_doesnt_exist(self):
        response = self.client.get('/users/partario/lists/')
        self.assertEqual(response.status_code, 404)

    def test_profile_list_filters_all_of_users_list(self):
        response = self.client.get('/users/{}/lists/'.format(self.user.username))
        list_set = list(ItemList.objects.filter(user=self.user))
        list_response = list(response.context['list_set'])

        self.assertEqual(list_response, list_set)

    def test_profile_list_fetches_active_list_if_active_and_no_slug(self):
        self.list1.active = False
        self.list1.save()
        response = self.client.get('/users/{}/lists/'.format(self.user.username))

        self.assertEqual(response.context['list_display'], self.list2)
        
    def test_profile_list_fetches_most_recent_list_if_no_slug_and_no_active(self):
        self.list1.active, self.list2.active = False, False
        self.list1.save()
        self.list2.save()
        list3 = ItemList.objects.create(user=self.user, active=False)
        response = self.client.get('/users/{}/lists/'.format(self.user.username))

        self.assertEqual(response.context['list_display'], list3)

    def test_profile_list_fetches_correct_list_if_slug_given(self):
        response = self.client.get('/users/{}/lists/?view={}'.format(self.user.username, self.list1.slug))
        self.assertEqual(response.context['list_display'], self.list1)


class ProfileEditListTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.client.force_login(self.user)
        self.list1 = ItemList.objects.create(user=self.user)
        self.list2 = ItemList.objects.create(user=self.user)

    def test_profile_edit_view_uses_correct_form(self):
        response = self.client.get('/users/{}/lists/{}/edit'.format(self.user.username, self.list1.slug))

        self.assertIsInstance(response.context['form'], ListDetailForm)

    def test_profile_edit_list_fetches_correct_list(self):
        response = self.client.get('/users/{}/lists/{}/edit'.format(self.user.username, self.list1.slug))

        self.assertEqual(response.context['title'], self.list1.title)

    def test_profile_does_not_fetch_complete_builds(self):
        build = ItemList.objects.create(user=self.user, complete=True)
        response = self.client.get('/users/{}/lists/?view={}'.format(self.user.username, build.slug))

        self.assertEqual(response.status_code, 404)

    def test_profile_edit_list(self):
        self.client.post('/users/{}/lists/{}/edit'.format(self.user.username, self.list1.slug),
                         data={'title': 'test title'})
        title = ItemList.objects.get(slug=self.list1.slug).title

        self.assertEqual(title, 'test title')

    def test_cannot_edit_other_users_lists_from_profile(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/users/{}/lists/{}/edit'.format(self.user.username, self.list1.slug),
                                    data={'title': 'test title'})

        self.assertEqual(response.status_code, 403)

    def test_profile_del_list(self):
        slug = self.list1.slug
        self.client.post('/users/{}/lists/{}/del'.format(self.user.username, self.list1.slug))
        self.assertFalse(ItemList.objects.filter(slug=slug))

    def test_profile_del_list_redirects_to_profile_lists(self):
        response = self.client.post('/users/{}/lists/{}/del'.format(self.user.username, self.list1.slug))
        self.assertRedirects(response, '/users/{}/lists/'.format(self.user.username))

    def test_cannot_del_other_users_lists_from_profile(self):
        self.client.logout()
        user2 = User.objects.create(username='Partario')
        self.client.force_login(user2)

        response = self.client.post('/users/{}/lists/{}/edit'.format(self.user.username, self.list1.slug),
                                    data={'title': 'test title'})

        self.assertEqual(response.status_code, 403)


class ProfileBuildTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.client.force_login(self.user)
        self.build1 = ItemList.objects.create(user=self.user, complete=True)
        self.build2 = ItemList.objects.create(user=self.user, complete=True)

    def test_view_retrieves_all_of_users_builds(self):
        response = self.client.get('/users/{}/builds/'.format(self.user.username))
        build_set = list(ItemList.objects.filter(user=self.user))
        build_response = list(response.context['build_set'])

        self.assertEqual(build_response, build_set)

    def test_profile_fetches_most_recent_build_if_no_slug(self):
        response = self.client.get('/users/{}/builds/'.format(self.user.username))
        self.assertEqual(response.context['build_display'], self.build2)

    def test_profile_fetches_correct_build_if_slug_given(self):
        response = self.client.get('/users/{}/builds/?view={}'.format(self.user.username, self.build1.slug))
        self.assertEqual(response.context['build_display'], self.build1)

    def test_profile_does_not_fetch_incomplete_lists(self):
        the_list = ItemList.objects.create(user=self.user)
        response = self.client.get('/users/{}/builds/?view={}'.format(self.user.username, the_list.slug))

        self.assertEqual(response.status_code, 404)

    def test_profile_del_build(self):
        slug = self.build1.slug
        self.client.post('/users/{}/builds/{}/del'.format(self.user.username, slug))
        self.assertFalse(ItemList.objects.filter(slug=slug).exists())

    def test_profile_del_build_redirects_to_profile_builds(self):
        response = self.client.post('/users/{}/builds/{}/del'.format(self.user.username, self.build1.slug))
        self.assertRedirects(response, '/users/{}/builds/'.format(self.user.username))

    def test_cannot_del_other_users_builds(self):
        self.client.logout()
        user2 = User.objects.create(username='partario')
        self.client.force_login(user2)

        response = self.client.post('/users/{}/builds/{}/del'.format(self.user.username, self.build1.slug))

        self.assertEqual(response.status_code, 403)


class AccountSettingsTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='Marfalo')
        self.profile = UserProfile.objects.create(user=self.user)
        self.client.force_login(self.user)

    def test_account_settings_uses_correct_template(self):
        response = self.client.get('/users/{}/settings/'.format(self.user.username))
        self.assertTemplateUsed(response, 'profiles/account_settings.html')

    def test_server_does_not_500_if_user_has_no_profile(self):
        self.profile.delete()
        response = self.client.get('/users/{}/settings/'.format(self.user.username))
        self.assertEqual(response.status_code, 200)

    def test_cannot_change_other_users_settings(self):
        self.client.logout()
        response = self.client.get('/users/{}/settings/'.format(self.user.username))
        self.assertEqual(response.status_code, 403)
