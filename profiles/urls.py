from django.conf.urls import url

from profiles import views
from lists import views as list_views

urlpatterns = [
    url(r'^(?P<username>[\w._@+-]{1,30})/$', views.UserProfileView.as_view(), name='profile_user'),
    url(r'^(?P<username>[\w._@+-]{1,30})/create$', views.create_profile, name='profile_create'),
    url(r'^(?P<username>[\w._@+-]{1,30})/edit$', views.edit_profile, name='profile_edit'),
    url(r'^(?P<username>[\w._@+-]{1,30})/lists/$', views.profile_lists, name='profile_lists'),
    url(r'^(?P<username>[\w._@+-]{1,30})/lists/(?P<slug>[A-Za-z0-9]{7})/edit$', list_views.edit_list, name='profile_edit_list'),
    url(r'^(?P<username>[\w._@+-]{1,30})/lists/(?P<slug>[A-Za-z0-9]{7})/del/confirm$', list_views.ListDelConfirmation.as_view(), name='profile_confirm_del_list'),
    url(r'^(?P<username>[\w._@+-]{1,30})/lists/(?P<slug>[A-Za-z0-9]{7})/del$', list_views.del_list, name='profile_del_list'),
    url(r'^(?P<username>[\w._@+-]{1,30})/builds/$', views.profile_builds, name='profile_builds'),
    url(r'^(?P<username>[\w._@+-]{1,30})/builds/(?P<slug>[A-Za-z0-9]{7})/del/confirm$', list_views.ListDelConfirmation.as_view(), name='profile_confirm_del_build'),
    url(r'^(?P<username>[\w._@+-]{1,30})/builds/(?P<slug>[A-Za-z0-9]{7})/del$', list_views.del_list, name='profile_del_build'),
    url(r'^(?P<username>[\w._@+-]{1,30})/settings/$', views.account_settings, name='account_settings'),
]
