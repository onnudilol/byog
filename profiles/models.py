from django.db import models
from django.conf import settings
from django.dispatch import receiver

from common.models import RestrictedImageField, IMAGE_TYPES
from parts.part_info.currency import CURRENCY_CHOICES

from PIL import Image
import os
import uuid

User = settings.AUTH_USER_MODEL


# From http://stackoverflow.com/questions/2673647/enforce-unique-upload-file-names-using-django
def get_file_path_user(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('img/profile/user/', filename)


def get_file_path_background(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('img/profile/background/', filename)


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    description = models.TextField(blank=True, default='', max_length=5000)
    currency = models.CharField(max_length=3, choices=CURRENCY_CHOICES, default='USD')
    user_image = RestrictedImageField(upload_to=get_file_path_user, content_types=IMAGE_TYPES,
                                      max_upload_size=5242880, null=True, blank=True)
    profile_image = RestrictedImageField(upload_to=get_file_path_background, content_types=IMAGE_TYPES,
                                         max_upload_size=5242880, null=True, blank=True)

    def __str__(self):
        return str(self.user)

    def save(self, *args, **kwargs):

        super().save(*args, **kwargs)

        # Resizes the image if it's larger than 256x256
        if self.user_image:
            filename = self.user_image
            thumb = Image.open(filename)
            if thumb.height > 256 or thumb.width > 256:
                thumb.thumbnail((256, 256))
                thumb.save(filename.path)


# From http://stackoverflow.com/a/16041527
@receiver(models.signals.post_delete, sender=UserProfile)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Deletes an uploaded image on disk when the associated model is deleted"""

    if instance.user_image:
        if os.path.isfile(instance.user_image.path):
            os.remove(instance.user_image.path)

    if instance.profile_image:
        if os.path.isfile(instance.profile_image.path):
            os.remove(instance.profile_image.path)


@receiver(models.signals.pre_save, sender=UserProfile)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem when corresponding partimage object is changed."""

    if not instance.pk:
        return False

    try:
        profile = UserProfile.objects.get(pk=instance.pk)
        old_user_image = profile.user_image
        old_profile_image = profile.profile_image

    except UserProfile.DoesNotExist:
        return False

    new_user_image = instance.user_image
    new_profile_image = instance.profile_image

    if old_user_image:
        if not old_user_image == new_user_image:
            if os.path.isfile(old_user_image.path):
                os.remove(old_user_image.path)

    if old_profile_image:
        if not old_profile_image == new_profile_image:
            if os.path.isfile(old_profile_image.path):
                os.remove(old_profile_image.path)
