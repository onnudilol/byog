from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.views.generic import DetailView

from profiles.models import UserProfile
from profiles.forms import UserProfileForm
from lists.models import ItemList
from lists.forms import ListDetailForm

from itertools import chain


class UserProfileView(DetailView):
    """
    Displays the user's profile information, if it exists.
    """

    model = UserProfile
    context_object_name = 'profile'
    template_name = 'profiles/profile.html'
    slug_url_kwarg = 'username'
    slug_field = 'user'

    def get_object(self, queryset=None):
        User = get_user_model()
        self.user = get_object_or_404(User.objects.select_related('userprofile'), username=self.kwargs['username'], is_active=True)

        # Sets profile in context if it exists, otherwise the profile page just displays generic information
        # and links to the user's lists and builds.
        try:
            profile = self.user.userprofile
            return profile
        except UserProfile.DoesNotExist:
            pass

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Sets the profile background image and user image in context if they exists,
        # otherwise the defaults are displayed in the rendered template.
        try:
            context['profile_image'] = self.user.userprofile.profile_image
        except UserProfile.DoesNotExist:
            pass

        try:
            context['user_image'] = self.user.userprofile.user_image
        except UserProfile.DoesNotExist:
            pass

        # Explicitly set these in the context instead of from attributes in the template to simplify extending templates
        context['username'] = self.kwargs['username']
        context['date_joined'] = self.user.date_joined
        context['profile_nav'] = 'about'

        return context


@login_required
def create_profile(request, username):
    """
    Creates a user profile.

    Args:
        request: The incoming HttpRequest.  GET request renders the form.  POST request processes form data.
        username: The user the profile is being created for

    Returns:
        form: An instance of UserProfileForm
        Redirects to the main user page.
        403s if a user tries to create another user's profile.
    """

    if request.user.username == username:

        if request.method == 'GET':
            form = UserProfileForm()
            return render(request, 'profiles/profile_create_edit.html', {'form': form})

        elif request.method == 'POST':
            form = UserProfileForm(request.POST, request.FILES)

            if form.is_valid():
                user_image = request.FILES.get('user_image')
                profile_image = request.FILES.get('profile_image')
                form.save(user=request.user, user_image=user_image, profile_image=profile_image)
                return redirect('profile_user', username=username)

    else:
        raise PermissionDenied


@login_required
def edit_profile(request, username):
    """
    Edits an existing user profile.

    Args:
        request: The incoming HttpRequest.  GET request renders the form.  POST request processes form data.
        username: The selected user's username.

    Returns:
        form: An instance of UserProfileForm
        403s if a user tries to edit another user's profile.
    """

    if request.user.username == username:
        profile = UserProfile.objects.get(user__username=username)
        form = None

        if request.method == 'GET':
            form = UserProfileForm(instance=profile)

        elif request.method == 'POST':
            form = UserProfileForm(request.POST, request.FILES)

            if form.is_valid():
                user_image = request.FILES.get('user_image')
                profile_image = request.FILES.get('profile_image')
                # If the user wants to delete their user image/profile image
                clear_user_img = request.POST.get('user_image-clear')
                clear_profile_img = request.POST.get('profile_image-clear')

                form.update(profile=profile, user_image=user_image, clear_user_img=clear_user_img,
                            profile_image=profile_image, clear_profile_img=clear_profile_img)

                return redirect('profile_user', username=username)

        return render(request, 'profiles/profile_create_edit.html', {'form': form})

    else:
        raise PermissionDenied


def profile_lists(request, username):
    """
    Similar to the main list view.
    Displays a list of all the user's list in a sidebar.
    Displays the user's most recent list in the display area by default,
    or whichever list is selected in the GET parameter.

    Args:
        request: The incoming HttpRequest
        username: The selected user's username

    Returns:
        list_set: The set of all the user's created lists
        list_display: The list to be displayed in the display area
        items: items: The combined items and custom items, sorted by category
    """

    User = get_user_model()
    user = get_object_or_404(User.objects.select_related('userprofile'), username=username, is_active=True)
    user_image = None
    profile_image = None

    list_set = ItemList.objects.filter(user=user, complete=False).prefetch_related('item_set__part', 'customitem_set')
    list_active = list_set.filter(active=True).first()
    list_display = None
    combined_items = None

    # Sets the profile background image and user image in context if they exist,
    # otherwise the defaults are displayed in the rendered template.
    try:
        profile = user.userprofile
        profile_image = profile.profile_image
        user_image = profile.user_image

    except UserProfile.DoesNotExist:
        pass

    # TODO Might want to recheck if the prefetch is done at the appropriate spot
    # If there's a view parameter, fetch that specific list
    # Otherwise, display the active list if it exists
    # Otherwise, display the most recent list
    # If the user has no lists, render the template and inform the user as such

    if list_set:

        if request.GET.get('view'):
            list_display = get_object_or_404(list_set, slug=request.GET['view'])
        elif list_active:
            list_display = list_active
        else:
            list_display = list_set.last()

        # Combine and sort items and custom items
        items = list_display.item_set.all()
        custom_items = list_display.customitem_set.all()
        combined_items = sorted(chain(items, custom_items), key=lambda item: (item.category, str(item)))

    return render(request, 'profiles/profile_lists.html', {'list_set': list_set,
                                                           'list_display': list_display,
                                                           'items': combined_items,
                                                           'user_image': user_image,
                                                           'profile_image': profile_image,
                                                           'profile_nav': 'lists',
                                                           'username': username})


def profile_builds(request, username):
    """
    Lists all the user's completed builds

    Args:
        request: The incoming HttpRequest
        username: The selected user's username

    Returns:

    """

    User = get_user_model()
    user = get_object_or_404(User.objects.select_related('userprofile'), username=username, is_active=True)
    user_image = None
    profile_image = None

    build_set = ItemList.objects.filter(user=user, complete=True).prefetch_related('item_set__part', 'customitem_set',
                                                                                   'buildimage_set')
    build_display = None
    combined_items = None

    # Sets the profile background image and user image in context if they exist,
    # otherwise the defaults are displayed in the rendered template.
    try:
        profile = user.userprofile
        profile_image = profile.profile_image
        user_image = profile.user_image

    except UserProfile.DoesNotExist:
        pass

    # Might want to recheck if the prefetch is done at the appropriate spot
    # If there's a view parameter, fetch that specific list
    # Otherwise, display the active list if it exists
    # Otherwise, display the most recent list
    # If the user has no lists, render the template and inform the user as such
    if build_set:

        if request.GET.get('view'):
            build_display = get_object_or_404(build_set, slug=request.GET['view'])
        else:
            build_display = build_set.last()

        items = build_display.item_set.all()
        custom_items = build_display.customitem_set.all()
        combined_items = sorted(chain(items, custom_items), key=lambda item: (item.category, str(item)))

    return render(request, 'profiles/profile_builds.html', {'build_set': build_set,
                                                            'build_display': build_display,
                                                            'items': combined_items,
                                                            'user_image': user_image,
                                                            'profile_image': profile_image,
                                                            'profile_nav': 'builds',
                                                            'username': username})


def account_settings(request, username):
    if request.user.username == username:

        user_image = None
        profile_image = None

        try:
            profile = UserProfile.objects.get(user=request.user)
            user_image = profile.user_image
            profile_image = profile.profile_image

        except ObjectDoesNotExist:
            pass

        return render(request, 'profiles/account_settings.html', {'profile_nav': 'settings',
                                                                  'username': username,
                                                                  'profile_image': profile_image,
                                                                  'user_image': user_image})
    else:
        raise PermissionDenied
