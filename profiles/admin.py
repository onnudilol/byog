from django.contrib import admin
from profiles.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    raw_id_fields = ['user']
    search_fields = ['user__username']

admin.site.register(UserProfile, UserProfileAdmin)
