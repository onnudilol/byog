from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Submit

from profiles.models import UserProfile


class UserProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # crispy-forms settings
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Fieldset(
                'Edit Profile',
                'description',
                'user_image',
                'profile_image',
            )
        )
        self.helper.add_input(Submit('submit', 'Save'))

    class Meta:
        model = UserProfile
        fields = ['description', 'user_image', 'profile_image']

    # Saves a profile with a user_image and profile_image from request.FILES to the user's profile
    def save(self, user, user_image, profile_image):
        profile = UserProfile.objects.create(user=user, description=self.cleaned_data['description'],
                                             user_image=user_image, profile_image=profile_image)
        return profile

    # Updates images if provided.  Clears them out if the user chooses to.
    def update(self, profile, user_image, clear_user_img, profile_image, clear_profile_img):
        profile.description = self.cleaned_data['description']

        if user_image is not None:
            profile.user_image = user_image
        elif clear_user_img == 'on':
            profile.user_image = None

        if profile_image is not None:
            profile.profile_image = profile_image
        elif clear_profile_img == 'on':
            profile.profile_image = None

        profile.save()
        return profile
