from django import template
from django.template.defaultfilters import stringfilter
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

import math

register = template.Library()


@register.filter
@stringfilter
def strip_underscore(words):
    word = words.replace('_slash_', ' / ').replace('_', ' ')
    return word


@register.filter(needs_autoescape=True)
def star_ratings(rating, autoescape=True):
    """
    Converts integer rating of a part review into stars

    Args:
        rating: The review's rating

    Returns:
        rating: List of stars

    """

    # Fontawesome stars
    star_glyph = '<i class="fa fa-star" aria-hidden="true"></i>'
    empty_star_glyph = '<i class="fa fa-star-o" aria-hidden="true"></i>'

    empty_stars = list(range(5 - rating))
    stars = list(range(rating))
    rating = list()

    for s in stars:
        rating.append(star_glyph)

    if empty_stars:
        for e in empty_stars:
            rating.append(empty_star_glyph)

    stars = ''.join(rating)

    return mark_safe(stars)


@register.filter(needs_autoescape=True)
def star_float(avg, autoescape=True):
    """
    Returns the float value of a part's average rating into stars

    Args:
        avg: The part's average rating

    Returns:
        rating: List of stars

    """

    # Fontawesome stars
    star_glyph = '<i class="fa fa-star" aria-hidden="true"></i>'
    empty_star_glyph = '<i class="fa fa-star-o" aria-hidden="true"></i>'
    half_star_glyph = '<i class="fa fa-star-half-o" aria-hidden="true"></i>'

    if avg is None:
        rating = [empty_star_glyph] * 5
        stars = ''.join(rating)
        return stars

    # Split the rating into integer and decimal parts
    dec_part, int_part = math.modf(avg)

    # If there's any decimal, there will be a half star
    if dec_part != 0.0:
        empty_stars = list(range(4 - int(int_part)))
    else:
        empty_stars = list(range(5 - int(int_part)))

    int_part = list(range(int(int_part)))
    rating = list()

    for i in int_part:
        rating.append(star_glyph)

    if dec_part:
        rating.append(half_star_glyph)

    for e in empty_stars:
        rating.append(empty_star_glyph)

    stars = ''.join(rating)

    return mark_safe(stars)



