from django.test import TestCase
from django.contrib.auth import get_user_model

from parts.forms import PartReviewForm
from parts.models import Part, PartReview
from lists.models import ItemList, Item

User = get_user_model()


class PartReviewFormTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='marfalo')
        self.part = Part.objects.create(manufacturer='PartCo', name='Part')
        self.build = ItemList.objects.create(user=self.user, complete=True)
        self.item = Item.objects.create(part=self.part, itemlist=self.build)

    def test_save_part_review(self):
        form = PartReviewForm(data={'rating': 3, 'review': 'nito'})

        self.assertTrue(form.is_valid())
        form.save(user=self.user, item=self.item, part=self.part, build=self.build)

        review = PartReview.objects.first()
        self.assertEqual(review.rating, 3)

    def test_update_part_review(self):
        review = PartReview.objects.create(user=self.user, part=self.part, item=self.item, build=self.build,
                                           rating=1, review='cool')
        form = PartReviewForm(data={'rating': 5, 'review': 'bad'})

        self.assertTrue(form.is_valid())
        form.update(review, self.item, self.build)
