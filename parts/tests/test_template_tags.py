import unittest
from parts.templatetags.parts_extras import strip_underscore, star_float, star_ratings


class PartTemplateTagTest(unittest.TestCase):

    def test_strip_underscores_strips_underscores(self):
        word = strip_underscore('words_words_words')
        self.assertEqual(word, 'words words words')

    def test_star_ratings_returns_correct_html(self):
        stars = star_ratings(3)
        rating = (
            '<i class="fa fa-star" aria-hidden="true"></i>'
            '<i class="fa fa-star" aria-hidden="true"></i>'
            '<i class="fa fa-star" aria-hidden="true"></i>'
            '<i class="fa fa-star-o" aria-hidden="true"></i>'
            '<i class="fa fa-star-o" aria-hidden="true"></i>'
        )

        self.assertEqual(stars, rating)

    def test_star_float_returns_correct_html_for_zero_decimal(self):
        stars = star_float(3.0)
        rating = (
            '<i class="fa fa-star" aria-hidden="true"></i>'
            '<i class="fa fa-star" aria-hidden="true"></i>'
            '<i class="fa fa-star" aria-hidden="true"></i>'
            '<i class="fa fa-star-o" aria-hidden="true"></i>'
            '<i class="fa fa-star-o" aria-hidden="true"></i>'
        )

        self.assertEqual(stars, rating)

    def test_star_float_returns_correct_html_for_nonzero_decimal(self):
        stars = star_float(3.5)
        rating = (
            '<i class="fa fa-star" aria-hidden="true"></i>'
            '<i class="fa fa-star" aria-hidden="true"></i>'
            '<i class="fa fa-star" aria-hidden="true"></i>'
            '<i class="fa fa-star-half-o" aria-hidden="true"></i>'
            '<i class="fa fa-star-o" aria-hidden="true"></i>'
        )

        self.assertEqual(stars, rating)

    def test_star_float_returns_empty_stars_if_no_avg_rating(self):
        stars = star_float(None)
        rating = (
            '<i class="fa fa-star-o" aria-hidden="true"></i>'
            '<i class="fa fa-star-o" aria-hidden="true"></i>'
            '<i class="fa fa-star-o" aria-hidden="true"></i>'
            '<i class="fa fa-star-o" aria-hidden="true"></i>'
            '<i class="fa fa-star-o" aria-hidden="true"></i>'
        )

        self.assertEqual(stars, rating)
