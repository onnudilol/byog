from django.test import TestCase
from django.utils.text import slugify
from django.contrib.auth import get_user_model

from parts.models import Part, PartReview, PriceBase, ReviewVote
from parts.views import increment_helpful
from lists.models import ItemList, Item

User = get_user_model()


class PartsListAllTest(TestCase):

    def test_parts_all_uses_correct_template(self):
        response = self.client.get('/parts/')
        self.assertTemplateUsed(response, 'parts/category_list_all.html')


class PartsCategoryTest(TestCase):

    def test_parts_category_uses_correct_template(self):
        response = self.client.get('/parts/cat/trigger/')
        self.assertTemplateUsed(response, 'parts/category.html')

    def test_part_category_view_sets_correct_context(self):
        response = self.client.get('/parts/cat/trigger/')
        self.assertEqual(response.context['thead'], ['Stage', 'Weight'])

    def test_part_category_404_if_category_does_not_exist(self):
        response = self.client.get('/parts/cat/sword/')
        self.assertEqual(response.status_code, 404)


class PartsDetailTest(TestCase):

    def setUp(self):
        self.part = Part.objects.create(category='trigger', manufacturer='test', name='tester', product_num='+35+')
        self.title = slugify(str(self.part))

    def test_parts_detail_view_uses_correct_template(self):

        response = self.client.get('/parts/{}/{}/'.format(self.part.slug, self.title))

        self.assertTemplateUsed(response, 'parts/part_detail.html')

    def test_parts_detail_view_retrieves_correct_part(self):
        wrong_part = Part.objects.create(category='suppresor', manufacturer='test2', name='test2', product_num='+35+2')
        response = self.client.get('/parts/{}/{}/'.format(self.part.slug, self.title))

        self.assertEqual(self.part, response.context['part'])
        self.assertNotEqual(wrong_part, response.context['part'])

    def test_view_redirects_to_correct_title_if_incorrect(self):
        response = self.client.get('/parts/{}/this-is-the-wrong-title/'.format(self.part.slug, self.title))

        self.assertRedirects(response, '/parts/{}/{}/'.format(self.part.slug, self.title))

    def test_404_if_part_does_not_exist(self):
        response = self.client.get('/parts/1234567/hhhhhhh/')
        self.assertEqual(response.status_code, 404)

    def test_reviews_are_returned_in_order_of_helpfulness(self):
        user = User.objects.create_user(username='marfalo')
        itemlist1 = ItemList.objects.create(user=user, complete=True)
        item1 = Item.objects.create(part=self.part, itemlist=itemlist1)
        review1 = PartReview.objects.create(rating=3, user=user, build=itemlist1, item=item1, part=self.part, helpful=100)

        user2 = User.objects.create_user(username='partario')
        itemlist2 = ItemList.objects.create(user=user2, complete=True)
        item2 = Item.objects.create(part=self.part, itemlist=itemlist2)
        review2 = PartReview.objects.create(rating=1, user=user2, build=itemlist2, item=item2, part=self.part, helpful=50)

        response = self.client.get('/parts/{}/{}/'.format(self.part.slug, self.title))

        self.assertEqual(response.context['reviews'][0], review1)
        self.assertEqual(response.context['reviews'][1], review2)


class PartVendorTest(TestCase):

    def setUp(self):
        self.part = Part.objects.create(manufacturer='PartCo', name='Part')
        self.price = PriceBase.objects.create(part=self.part, price=5.00, vendor='PA', url='http://google.com')

    def test_redirect_to_vendor_url(self):
        response = self.client.get('/parts/{}/vendor/{}/'.format(self.part.slug, self.price.vendor))
        self.assertEqual(response.status_code, 302)

    def test_404_if_vendor_url_doesnt_exist(self):
        response = self.client.get('/parts/{}/vendor/{}/'.format(self.part.slug, 'NOVENDOR'))
        self.assertEqual(response.status_code, 404)


class ReviewTest(TestCase):

    def setUp(self):
        self.part = Part.objects.create(manufacturer='PartCo', name='Party')
        self.user = User.objects.create_user(username='marfalo')
        self.itemlist = ItemList.objects.create(user=self.user, complete=True)
        self.item = Item.objects.create(part=self.part, itemlist=self.itemlist)
        self.review = PartReview.objects.create(rating=3, user=self.user, build=self.itemlist, item=self.item,
                                                part=self.part, helpful=100)
        self.client.force_login(self.user)

    def test_vote_new(self):
        self.client.post('/parts/review/{}/rate'.format(self.review.id), {'helpful': 'true'},
                         HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        review = PartReview.objects.first()

        self.assertEqual(review.helpful, 101)

    def test_vote_edit(self):
        ReviewVote.objects.create(user=self.user, review=self.review, helpful=True)
        self.client.post('/parts/review/{}/rate'.format(self.review.id), {'helpful': 'false'},
                         HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        review = PartReview.objects.first()
        self.assertEqual(review.helpful, 99)
        self.assertEqual(review.unhelpful, 1)

    def test_review_vote_405_if_not_POST(self):
        ReviewVote.objects.create(user=self.user, review=self.review, helpful=True)
        response = self.client.get('/parts/review/{}/rate'.format(self.review.id), {'helpful': 'false'},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 405)

    def test_increment_helpful_updates_review_score_if_new_vote_created(self):
        vote = ReviewVote.objects.create(user=self.user, review=self.review, helpful=True)
        increment_helpful(self.review, vote, helpful_bool=True, created=True)
        review = PartReview.objects.get(id=self.review.id)
        self.assertEqual(review.helpful, 101)

    def test_increment_helpful_updates_vote_helpful_boolean_if_not_equal(self):
        vote = ReviewVote.objects.create(user=self.user, review=self.review, helpful=True)
        increment_helpful(self.review, vote, helpful_bool=False, created=False)
        self.assertEqual(vote.helpful, False)

    def test_increment_helpful_changes_review_helpful_score_if_vote_changed(self):
        vote = ReviewVote.objects.create(user=self.user, review=self.review, helpful=True)
        increment_helpful(self.review, vote, helpful_bool=False, created=False)
        review = PartReview.objects.get(id=self.review.id)
        self.assertEqual(review.helpful, 99)

    def test_review_detail_fetches_correct_review(self):
        response = self.client.get('/parts/{}/{}/reviews/{}/'.format(self.part.slug, slugify(self.part.name), self.review.id))
        self.assertEqual(response.context['review'], self.review)

    def test_review_detail_creates_disqus_context_if_user_authenticated(self):
        response = self.client.get('/parts/{}/{}/reviews/{}/'.format(self.part.slug, slugify(self.part.name), self.review.id))
        self.assertTrue(response.context['disqus'])

    def test_review_detail_does_not_create_disqus_context_if_user_not_authenticated(self):
        self.client.logout()
        response = self.client.get('/parts/{}/{}/reviews/{}/'.format(self.part.slug, slugify(self.part.name), self.review.id))
        self.assertFalse(response.context['disqus'])

    def test_404_if_review_does_not_exist(self):
        response = self.client.get('/parts/{}/{}/reviews/11/'.format(self.part.slug, slugify(self.part.name)))
        self.assertEqual(response.status_code, 404)
