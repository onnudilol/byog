from django.test import TestCase
from django.contrib.auth.models import User
from django.utils.text import slugify

from parts.models import Part, PriceBase, PartReview
from lists.models import ItemList, Item


class PartTest(TestCase):

    def setUp(self):
        self.part = Part.objects.create(manufacturer='PartCo', name='Part')
        self.low_price = PriceBase.objects.create(part=self.part, vendor='PA', url='example.com', price=1)
        self.high_price = PriceBase.objects.create(part=self.part, vendor='MUSA', url='example.com', price=9999)
        self.part.save()

    def test_part_save_generates_slug(self):
        self.assertTrue(self.part.slug)

    def test_part_saves_lowest_price(self):
        self.assertEqual(self.part.lowest_price, 1)

    def test_lowest_price_vendor(self):
        self.assertEqual(self.part.lowest_price_vendor, 'PA')

    def test_part_product_num_set_to_none_if_given_empty_string(self):
        part = Part.objects.create(manufacturer='PartCo', name='Part', product_num='')
        self.assertIsNone(part.product_num)

    def test_part_average_rating_is_correct(self):
        user = User.objects.create_user(username='marfalo')
        itemlist1 = ItemList.objects.create(user=user, complete=True)
        item1 = Item.objects.create(part=self.part, itemlist=itemlist1)
        PartReview.objects.create(rating=3, user=user, build=itemlist1, item=item1, part=self.part)

        user2 = User.objects.create_user(username='partario')
        itemlist2 = ItemList.objects.create(user=user2, complete=True)
        item2 = Item.objects.create(part=self.part, itemlist=itemlist2)
        PartReview.objects.create(rating=1, user=user2, build=itemlist2, item=item2, part=self.part)

        self.assertEqual(self.part.avg_rating, (2.0, 2))

    def test_get_absolute_url(self):
        self.assertEqual(self.part.get_absolute_url(), '/parts/{}/{}/'.format(self.part.slug, slugify(str(self.part))))


class PartReviewTest(TestCase):

    def setUp(self):
        self.part = Part.objects.create(manufacturer='PartCo', name='Party')
        self.user = User.objects.create_user(username='marfalo')
        self.itemlist = ItemList.objects.create(user=self.user, complete=True)
        self.item = Item.objects.create(part=self.part, itemlist=self.itemlist)
        self.review = PartReview.objects.create(rating=3, user=self.user, build=self.itemlist, item=self.item,
                                                part=self.part, helpful=100)

    def test_get_absolute_url(self):
        self.assertEqual(self.review.get_absolute_url(), '/parts/{}/{}/reviews/{}/'.format(self.part.slug, slugify(str(self.part)), self.review.id))
