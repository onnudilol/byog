from collections import OrderedDict

# Table head columns/Sidebar filters for category views
TABLE_SIDEBAR_CATEGORIES = {
    'complete_ar15': {
        'title': 'Complete Rifle (AR-15)',
        'thead': ['Caliber', 'Barrel Length'],
        'columns': ['caliber_slash_gauge', 'barrel_length'],
        'manufacturers': ['GunCo', 'SwordCo'],
        'specs': OrderedDict([
            ('caliber', ['5.56 mm', '7.62 mm']),
            ('barrel_length', ['16 in', '26 in']),
        ])
    },
    'lower': {
        'title': 'Lower Receiver',
        'thead': ['Caliber', 'Material', 'Complete'],
        'columns': ['caliber', 'material', 'complete'],
        'manufacturers': [],
        'specs': OrderedDict([
            ('caliber', ['5.56 mm', '7.62 mm']),
            ('material', ['metal', 'plastic'])
        ])
    },
    'lower_parts_kit': {
        'title': 'Lower Parts Kit',
        'thead': [],
        'columns': [],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'trigger': {
        'title': 'Trigger',
        'thead': ['Stage', 'Weight'],
        'columns': ['stage', 'weight'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'trigger_guard': {
        'title': 'Trigger Guard',
        'thead': [],
        'columns': [],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'stock': {
        'title': 'Stock',
        'thead': ['Material', 'Length'],
        'columns': ['material', 'length'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'upper_stripped': {
        'title': 'Upper Receiver (Stripped)',
        'thead': ['Caliber', 'Material', 'Complete'],
        'columns': ['caliber', 'material', 'complete'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'upper_complete': {
        'title': 'Upper Receiver (Complete)',
        'thead': ['Caliber', 'Material', 'Complete'],
        'columns': ['caliber', 'material', 'complete'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'upper_parts_kit': {
        'title': 'Upper Parts Kit',
        'thead': [],
        'columns': [],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'barrel': {
        'title': 'Barrel',
        'thead': ['Caliber', 'Length', 'Twist Rate'],
        'columns': ['caliber_slash_gauge', 'barrel_length', 'twist_rate'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'flash_hider': {
        'title': 'Flash Hider',
        'thead': ['Caliber', 'Material', 'Length'],
        'columns': ['caliber', 'material', 'length'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'muzzle_brake': {
        'title': 'Muzzle Brake',
        'thead': ['Caliber', 'Material', 'Length'],
        'columns': ['caliber', 'material', 'length'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'suppressor': {
        'title': 'Suppressor',
        'thead': ['Caliber', 'Decibels'],
        'columns': ['caliber', 'decibels'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'bolt_carrier_group': {
        'title': 'Bolt Carrier Group',
        'thead': ['Caliber', 'Material'],
        'columns': ['caliber', 'material'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'gas_block': {
        'title': 'Gas Block',
        'thead': ['Material', 'Adjustable'],
        'columns': ['material', 'adjustable'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'charging_handle': {
        'title': 'Charging Handle',
        'thead': ['Material', 'Handedness'],
        'columns': ['material', 'handedness'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'handguard_rails': {
        'title': 'Handguard/Rails',
        'thead': ['Material'],
        'columns': ['material'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'sight': {
        'title': 'Sight',
        'thead': ['Position', 'Fixed/Folding', 'Illuminated', 'Type'],
        'columns': ['position', 'fixed_folding', 'illuminated', 'type'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'red_dot': {
        'title': 'Red Dot Sights',
        'thead': ['MOA', 'Reticle', 'Mount'],
        'columns': ['moa', 'reticle', 'mount'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'magnifier': {
        'title': 'Magnifier',
        'thead': ['Magnification', 'Mount'],
        'columns': ['magnification', 'mount'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'rds_mount': {
        'title': 'Red Dot Mount',
        'thead': ['Optic Type', 'Attachment Type'],
        'columns': ['optic_type', 'attachment_type'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'scope': {
        'title': 'Scope',
        'thead': ['Magnification', 'Reticle'],
        'columns': ['magnification', 'reticle'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'scope_mount': {
        'title': 'Scope Mount',
        'thead': ['Optic Type', 'Attachment Type'],
        'columns': ['optic_type', 'attachment_type'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'grip': {
        'title': 'Grip',
        'thead': ['Material', 'Finish'],
        'columns': ['material', 'finish'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'flashlight': {
        'title': 'Flashlight',
        'thead': ['Mount', 'Brightness'],
        'columns': ['mount', 'brightness'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'laser': {
        'title': 'Laser',
        'thead': ['Color'],
        'columns': ['color'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'bipod': {
        'title': 'Bipod',
        'thead': ['Mount', 'Material'],
        'columns': ['mount_style', 'material'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'rail_cover': {
        'title': 'Rail Cover',
        'thead': ['Material', 'Platform'],
        'columns': ['material', 'platform'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'magazine': {
        'title': '',
        'thead': ['Caliber', 'Capacity'],
        'columns': ['caliber', 'capacity'],
        'manufacturers': [],
        'specs': OrderedDict([])
    },
    'other': {
        'title': 'Other',
        'thead': [],
        'columns': [],
        'manufacturers': [],
        'specs': OrderedDict([])
    }
}
