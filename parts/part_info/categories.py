
CATEGORY_CHOICES = (

    ('Complete Rifles', (
        ('complete_ar15', 'Complete Rifle (AR-15)'),
    )
     ),

    ('Lowers (Core)', (
        ('lower', 'Lower Receiver'),
        ('lower_parts_kit', 'Lower Parts Kit'),
    )
     ),

    ('Lowers Components', (
        ('trigger', 'Trigger'),
        ('trigger_guard', 'Trigger Guard'),
        ('stock', 'Stock'),
    )
     ),

    ('Uppers (Core)', (
        ('upper_stripped', 'Upper Receiver (Stripped)'),
        ('upper_complete', 'Upper Receiver (Complete)'),
        ('upper_parts_kit', 'Upper Parts Kit'),
    )
     ),

    ('Uppers Components', (
        ('barrel', 'Barrel'),
        ('flash_hider', 'Flash Hider'),
        ('muzzle_brake', 'Muzzle Brake'),
        ('suppressor', 'Suppressor'),
        ('bolt_carrier_group', 'Bolt Carrier Group'),
        ('gas_block', 'Gas Block'),
        ('charging_handle', 'Charging Handle'),
        ('handguard_rails', 'Handguard/Rails'),
    )
     ),

    ('Optics', (
        ('sight', 'Sights'),
        ('red_dot', 'Red Dot'),
        ('rds_mount', 'Red Dot Mount'),
        ('magnifier', 'Magnifier'),
        ('scope', 'Scope'),
        ('scope_mount', 'Scope Mount'),
    )
     ),

    ('Accessories', (
        ('grip', 'Grip'),
        ('flashlight', 'Flashlight'),
        ('laser', 'Laser'),
        ('bipod', 'Bipod'),
        ('rail_cover', 'Rail Cover'),
        ('magazine', 'Magazine'),
    )
     ),

    ('Other', (
        ('other', 'Other'),
    )
     ),
)

CATEGORY_CHOICES_LIST = [
    'complete_ar15',
    'lower',
    'lower_parts_kit',
    'trigger',
    'trigger_guard',
    'stock',
    'upper_stripped',
    'upper_complete',
    'upper_parts_kit',
    'barrel',
    'flash_hider',
    'muzzle_brake',
    'suppressor',
    'bolt_carrier_group',
    'gas_block',
    'charging_handle',
    'handguard_rails',
    'sight',
    'red_dot',
    'rds_mount',
    'magnifier',
    'scope',
    'scope_mount',
    'grip',
    'flashlight',
    'laser',
    'bipod',
    'rail_cover',
    'magazine',
    'other',
]

CATEGORY_CHOICES_CORE = [
    'Lower Receiver',
    'Trigger',
    'Stock',
    'Upper Receiver (Stripped)',
    'Barrel',
    'Flash Hider',
    'Muzzle Brake',
    'Suppressor',
    'Bolt Carrier Group',
    'Gas Block',
    'Charging Handle',
    'Handguard/Rails',
]

CATEGORY_CHOICES_CORE_FIELDS = {
    'Lower Receiver': 'lower',
    'Trigger': 'trigger',
    'Stock': 'stock',
    'Upper Receiver (Stripped)': 'upper_stripped',
    'Barrel': 'barrel',
    'Flash Hider': 'flash_hider',
    'Muzzle Brake': 'muzzle_brake',
    'Suppressor': 'suppressor',
    'Bolt Carrier Group': 'bolt_carrier_group',
    'Gas Block': 'gas_block',
    'Charging Handle': 'charging_handle',
    'Handguard/Rails': 'handguard_rails',
}
