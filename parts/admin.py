from django.contrib import admin
from parts.models import Part, PriceBase, PricePromo, PartImage, PartReview


class PriceInline(admin.TabularInline):
    model = PriceBase
    readonly_fields = ['date']
    extra = 3
    show_change_link = True


class PromoInline(admin.TabularInline):
    model = PricePromo
    extra = 3
    readonly_fields = ['date_start']
    fields = ['base', 'discount', 'date_start', 'date_end', 'description', 'expired']


class partimageInline(admin.TabularInline):
    model = PartImage
    extra = 3


class PartAdmin(admin.ModelAdmin):
    readonly_fields = ['slug']
    inlines = [PriceInline, partimageInline]
    search_fields = ['manufacturer', 'name']
    list_filter = ['category', 'manufacturer']
    list_display = ['__str__', 'category', 'manufacturer', 'model', 'product_num']

    class Media:
        js = ['js/list_filter_collapse.js']


class RatingAdmin(admin.ModelAdmin):
    list_display = ['user', 'part', 'rating']
    raw_id_fields = ['user', 'part', 'build', 'item']


class PriceAdmin(admin.ModelAdmin):
    list_display = ['part', 'price']
    raw_id_fields = ['part']
    inlines = [PromoInline]


admin.site.register(Part, PartAdmin)
admin.site.register(PartReview, RatingAdmin)
admin.site.register(PriceBase, PriceAdmin)
