from django.forms import ModelForm
from parts.models import PartReview

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Submit, Field, HTML


class PartReviewForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # crispy forms settings
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Fieldset(
                '{{ part }} review',
                Field('rating', type='hidden'),
                HTML(
                    """
                    <div class='star-rating'>
                        <label>Rating</label>
                        <div id='rateYo'></div>
                        {% if form.errors %}
                            <p class="text-danger"><b>This field is required.</b></p>
                        {% endif %}
                    </div>
                 """
                ),
                'review',
            )
        )
        self.helper.add_input(Submit('submit', 'Save'))

    class Meta:
        model = PartReview
        fields = ['rating', 'review']

    def save(self, user, item, part, build):
        return PartReview.objects.create(user=user, build=build, item=item, part=part,
                                         rating=self.cleaned_data['rating'],
                                         review=self.cleaned_data['review'])

    def update(self, review, item, build):
        review.item = item
        review.build = build
        review.rating = self.cleaned_data['rating']
        review.review = self.cleaned_data['review']
        review.save()
