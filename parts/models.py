import os
import uuid
import decimal

from PIL import Image
from django.contrib.postgres.fields import JSONField
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Min, Avg
from django.dispatch import receiver
from django.utils.crypto import get_random_string
from django.utils.text import slugify
from sorl.thumbnail import ImageField

from byog.settings import AUTH_USER_MODEL
from parts.part_info.categories import CATEGORY_CHOICES
from parts.part_info.vendors import VENDOR_CHOICES
from parts.part_info.currency import CURRENCY_CHOICES


# From http://stackoverflow.com/questions/2673647/enforce-unique-upload-file-names-using-django
def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('img/parts/', filename)


class Part(models.Model):
    category = models.CharField(max_length=30, choices=CATEGORY_CHOICES, default='')
    manufacturer = models.CharField(max_length=100, default='')
    name = models.CharField(max_length=250, default='')
    product_num = models.CharField(max_length=50, null=True, unique=True, blank=True)
    specs = JSONField(default=dict(), blank=True)
    slug = models.CharField(unique=True, max_length=7)
    lowest_price = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    image = ImageField(upload_to=get_file_path, null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = get_random_string(length=7)

        if not self.lowest_price:
            self.lowest_price = self.pricebase_set.all().aggregate(Min('price'))['price__min']

        if not self.image:
            try:
                self.image = self.partimage_set.first().image
            except AttributeError:
                pass

        if self.product_num == '':
            self.product_num = None

        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('parts_detail', kwargs={'slug': self.slug, 'title': slugify(self.__str__())})

    @property
    def lowest_price_vendor(self):
        try:
            price = self.pricebase_set.all().order_by('price').first().vendor
            return price
        except AttributeError:
            return None

    def get_price(self, currency='USD'):
        currency_field = {'USD': self.lowest_price}
        currency_symbol = dict((currency, symbol) for currency, symbol in CURRENCY_CHOICES)

        if currency_field[currency]:
            return "{}{}".format(currency_symbol[currency], currency_field[currency])
        else:
            return None

    @property
    def avg_rating(self):
        reviews = self.partreview_set.filter(build__complete=True)
        if reviews:
            rating = float("{0:.2f}".format(reviews.aggregate(Avg('rating'))['rating__avg']))
        else:
            rating = 0.0

        total = reviews.count()
        average = (rating, total)
        return average


class PriceBase(models.Model):

    part = models.ForeignKey('Part', on_delete=models.CASCADE, default=None)
    vendor = models.CharField(max_length=15, choices=VENDOR_CHOICES)
    currency = models.CharField(max_length=3, choices=CURRENCY_CHOICES, default='USD')
    price = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    date = models.DateField(auto_now_add=True)
    url = models.URLField()

    def __str__(self):
        return self.vendor + ': {}'.format(self.get_currency_display()) + str(self.price)


@receiver(models.signals.post_save, sender=PriceBase)
def auto_update_part_lowest_price(sender, instance, **kwargs):
    """
    Updates a part's lowest_price field if the newly saved base price is lower than the part's current lowest price
    """

    if instance.price:
        if instance.part.lowest_price is None or decimal.Decimal(instance.price) < decimal.Decimal(instance.part.lowest_price):
            instance.part.lowest_price = instance.price
            instance.part.save()


class PricePromo(models.Model):
    base = models.ForeignKey('PriceBase', on_delete=models.CASCADE)
    discount = models.DecimalField(max_digits=6, decimal_places=2)
    currency = models.CharField(max_length=3, choices=CURRENCY_CHOICES, default='USD')
    date_start = models.DateField(auto_now_add=True)
    date_end = models.DateField()
    expired = models.BooleanField(default=False)
    description = models.TextField(max_length=500, default='', blank=True)


class PartReview(models.Model):
    SCORE_CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )

    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE)
    part = models.ForeignKey('Part', on_delete=models.CASCADE)
    build = models.ForeignKey('lists.ItemList', on_delete=models.CASCADE)
    item = models.OneToOneField('lists.Item', on_delete=models.CASCADE)
    rating = models.SmallIntegerField(choices=SCORE_CHOICES)
    review = models.TextField(default='', max_length=5000)
    helpful = models.IntegerField(default=0)
    unhelpful = models.IntegerField(default=0)

    def __str__(self):
        return "{} - {}".format(self.part, self.user.username)

    def get_absolute_url(self):
        part = self.part
        return reverse('parts_review_detail', kwargs={'review_id': self.id, 'slug': part.slug, 'title': slugify(part.name)})

    class Meta:
        unique_together = (
            ('part', 'user')
        )

    @property
    def percent_helpful(self):

        if self.helpful + self.unhelpful > 0:

            return "{} of {} user(s) ({:.0%}) found this review helpful.".format(
                self.helpful, (self.helpful + self.unhelpful), (self.helpful / (self.helpful + self.unhelpful))
            )

        else:
            return None


class ReviewVote(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE)
    review = models.ForeignKey('PartReview', on_delete=models.CASCADE)
    helpful = models.BooleanField()

    class Meta:
        unique_together = (
            ('review', 'user')
        )


class PartImage(models.Model):
    part = models.ForeignKey('Part', on_delete=models.CASCADE, default=None)
    image = ImageField(upload_to=get_file_path, blank=True)

    def save(self, *args, **kwargs):

        super().save(*args, **kwargs)

        # Resizes the image if it's larger than 500x400
        if self.image:
            filename = self.image
            thumb = Image.open(filename)
            if thumb.height > 400 or thumb.width > 500:
                thumb.thumbnail((500, 400))
                thumb.save(filename.path)


# From http://stackoverflow.com/a/16041527
@receiver(models.signals.post_delete, sender=PartImage)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Deletes an uploaded image on disk when the associated model is deleted"""

    if instance.image:

        if instance.part.image == instance.image:
            instance.part.image = None
            instance.part.save()

        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


@receiver(models.signals.pre_save, sender=PartImage)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem when corresponding partimage object is changed."""

    if not instance.pk:
        return False

    try:
        old_image = PartImage.objects.get(pk=instance.pk).image

    except PartImage.DoesNotExist:
        return False

    new_image = instance.image

    if old_image:

        if not old_image == new_image:

            if instance.part.image == old_image:
                instance.part.image = new_image
                instance.part.save()

            if os.path.isfile(old_image.path):
                os.remove(old_image.path)


@receiver(models.signals.post_save, sender=PartImage)
def auto_add_part_image(sender, instance, **kwargs):
    """Adds an image to a part if one does not already exist"""

    if not instance.part.image:
        instance.part.image = instance.image
        instance.part.save()
