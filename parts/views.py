from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.db.models import Avg, Prefetch, Case, When, Max, Func
from django.db.models.expressions import RawSQL, Q, Func
from django.http import Http404, HttpResponse, HttpResponseNotAllowed
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.text import slugify
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import TemplateView, ListView

from django_datatables_view.base_datatable_view import BaseDatatableView

from parts.models import Part, PriceBase, PartReview, ReviewVote
from parts.part_info.table_sidebar_categories import TABLE_SIDEBAR_CATEGORIES
from parts.part_info.categories import CATEGORY_CHOICES
from common.sso import get_disqus_sso

from decimal import Decimal


class CategoryAllView(TemplateView):
    """
    Displays all part categories.
    """

    template_name = 'parts/category_list_all.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = CATEGORY_CHOICES
        context['navbar'] = 'parts'
        return context


class PartCategoryView(TemplateView):
    """
    Renders template for a part category
    """

    template_name = 'parts/category.html'
    sidebar_filter = TABLE_SIDEBAR_CATEGORIES

    def get_context_data(self, **kwargs):
        try:
            category = self.kwargs['category']
            sidebar = self.sidebar_filter[category]
            max_price = Part.objects.filter(category=self.kwargs['category']).aggregate(Max('lowest_price'))
        except KeyError:
            raise Http404('Category does not exist')

        context = super().get_context_data(**kwargs)
        context['navbar'] = 'parts'
        context['category'] = category
        context['max_price'] = max_price['lowest_price__max']
        context['title'] = sidebar['title']
        context['thead'] = sidebar['thead']
        context['manufacturers'] = sidebar['manufacturers']
        context['specs'] = sidebar['specs']

        return context


@ensure_csrf_cookie
def part_detail(request, slug, title):
    """
    Fetches the part matching the slug parameter.  Corrects the title if it's incorrect.

    Args:
        request: The incoming HttpRequest
        slug: The selected part's slug
        title: The str representation of the part

    Returns:
        part: The selected part
        404 if the part does not exist
        Redirects with the correct title if the request's title does not match the str representation of the part
    """

    part = get_object_or_404(Part.objects.prefetch_related('partreview_set', 'pricebase_set',
                                                           'partimage_set'), slug=slug)
    avg_rating, total_reviews = part.avg_rating
    correct_title = slugify(str(part))
    reviews = part.partreview_set.filter(build__complete=True).prefetch_related('user__userprofile', 'build').order_by('-helpful')[:10]

    if title != correct_title:
        return redirect('/parts/{}/{}/'.format(slug, correct_title))

    return render(request, 'parts/part_detail.html', {'part': part,
                                                       'avg_rating': avg_rating,
                                                       'total_reviews': total_reviews,
                                                       'reviews': reviews,
                                                       'navbar': 'parts'})


class PartReviewList(ListView):
    model = PartReview
    paginate_by = 25
    template_name = 'parts/part_review_list.html'
    context_object_name = 'reviews'

    def get_queryset(self):
        self.part = Part.objects.filter(slug=self.kwargs['slug']).first()
        reviews = PartReview.objects.filter(part=self.part).prefetch_related('user__userprofile', 'part', 'build').order_by('-helpful')
        return reviews

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['part'] = self.part
        return context


def part_review_detail(request, slug, title, review_id):
    review = get_object_or_404(PartReview.objects.prefetch_related('user__userprofile', 'part'), id=review_id)
    site = "http://{}/parts/{}/{}/reviews/{}/".format(Site.objects.get_current().domain, slug, title, review.id)
    disqus = ''

    if request.user.is_authenticated():
        disqus = get_disqus_sso(request.user, site, review.id)

    return render(request, 'parts/part_review_detail.html', {'review': review, 'site': site, 'disqus': disqus})


def part_vendor(request, slug, vendor):
    """
    Redirects the user to the vendor's page for a part.

    Args:
        request: The incoming HttpRequest
        slug: The selected part's slug
        vendor: The selected vendor

    Returns:
        Redirect to the vendor's page for a part
        404 if the vendor does not have a page for the part
    """

    part = Part.objects.get(slug=slug)
    try:
        url = PriceBase.objects.get(vendor=vendor, part=part).url
        return redirect(url)
    except PriceBase.DoesNotExist:
        raise Http404


def increment_helpful(review, vote, helpful_bool, created):
    """
    Helper function to increment/decrement a review's helpfulness score

    Args:
        review: The review to score
        vote: The vote object
        helpful_bool: The helpfulness boolean in the request POST body
        created: Whether or not a new vote object was created

    Returns:
        Nothing.  Exits the helper function.
    """

    # If a new vote object is created,
    # simply update the review's helpful/unhelpful score based on the helpful boolean
    if created:
        if vote.helpful:
            review.helpful += 1
        else:
            review.unhelpful += 1

        review.save()
        return

    # If there is an existing vote object:
    #   if the object's helpful bool does not match the POST helpful bool:
    #       update the vote's helpful bool.
    #       Increment/decrement the review's helpful/unhelpful score to reflect the new vote
    else:
        if vote.helpful is not helpful_bool:
            vote.helpful = helpful_bool
            vote.save()

            if helpful_bool:
                review.unhelpful -= 1
                review.helpful += 1

            else:
                review.unhelpful += 1
                review.helpful -= 1

            review.save()
            return


@login_required
def review_vote(request, review_id):
    """
    Marks a review as helpful or unhelpful

    Args:
        request: The incoming HttpRequest, should be AJAX
        review_id: The selected review's id

    Returns:
        Empty HttpResponse.  Updates the reviews helpful/unhelpful score.
    """

    if request.is_ajax() and request.method == 'POST':
        # Convert js booleans to python booleans
        if request.POST.get('helpful', None) == 'true':
            helpful_boolean = True
        else:
            helpful_boolean = False

        review = get_object_or_404(PartReview.objects.prefetch_related('part'), id=review_id)
        vote, created = ReviewVote.objects.get_or_create(user=request.user, review=review,
                                                         defaults={'helpful': helpful_boolean})

        increment_helpful(review, vote, helpful_boolean, created)

        return HttpResponse()

    else:
        return HttpResponseNotAllowed('POST')


class RoundInt(Func):
    """
    Floors annotated AVG float
    """
    function = 'ROUND'
    template = '%(function)s(%(expressions)s, 2)'


# From https://www.isotoma.com/blog/2015/11/23/sorting-querysets-with-nulls-in-django/
class IsNull(Func):
    template = '%(expressions)s IS NULL'


class PartsCategoryDT(BaseDatatableView):
    """
    Customised BaseDataTableView.

    thead_base are the columns that are common to every category.
    part_columns are category specific columns.
    get_thead() returns thead if it exists, or calls create_thead().
    create_thead() inserts part specific columns into thead starting at the 1st index and returns it as a new list.
    get_specs() returns a list with common part column data, and then inserts category specific column data,
    starting at the 1st index.
    ordering() has been hacked to sort JSONFields.
    """

    model = Part
    max_display_length = 3000
    thead_base = ['name', 'rating_avg', 'lowest_price']
    thead = None
    part_columns = TABLE_SIDEBAR_CATEGORIES

    def create_thead(self):
        columns = self.part_columns[self.kwargs['category']]['columns']
        thead_new = self.thead_base[:]

        for column in columns[::-1]:
            thead_new.insert(1, 'specs__{}'.format(column))

        self.thead = thead_new
        return self.thead

    def get_thead(self):
        if self.thead:
            return self.thead
        else:
            return self.create_thead()

    def get_columns(self):
        self.columns = self.get_thead()
        return self.columns

    def get_part_columns(self):
        return self.part_columns[self.kwargs['category']]['columns']

    def get_specs(self):
        return self.part_columns[self.kwargs['category']]['specs']

    def get_order_columns(self):
        self.order_columns = self.get_thead()
        return self.order_columns

    def get_initial_queryset(self):
        # Prefetch and average part reviews only when they are from complete builds
        return Part.objects.filter(category=self.kwargs['category']).prefetch_related(
            Prefetch('partreview_set',
                     queryset=PartReview.objects.filter(build__complete=True),
                     to_attr='active_reviews')).annotate(
            rating_avg=RoundInt(Avg(Case(When(partreview__build__complete=True, then='partreview__rating')))))

    def filter_queryset(self, qs):
        # qs_filters is a list of Q objects
        qs_filters = list()

        # Create Q objects for search bar parameters
        search = self.request.POST.get('search[value]', None)

        if search:
            qs_filters.append(Q(name__icontains=search) | Q(manufacturer__icontains=search))

        # Creates Q objects for prices
        prices = self.request.POST.getlist('filters[prices][]')

        if prices:
            q_prices = Q()
            q_prices &= Q(lowest_price__gte=Decimal(prices[0]))
            q_prices &= Q(lowest_price__lte=Decimal(prices[1]))

            if prices[0] == '0':
                q_prices |= Q(lowest_price=None)

            qs_filters.append(q_prices)

        # Create Q objects for filtering by rating
        ratings = self.request.POST.getlist('filters[rating][]')

        if ratings:
            q_rating = Q()

            for rate in ratings:
                q_rating |= Q(rating_avg=rate)

            qs_filters.append(q_rating)

        # Create Q objects for manufacturer checkbox filters
        manufacturer = self.request.POST.getlist('filters[manufacturer][]')

        if manufacturer:
            q_man = Q()

            for man in manufacturer:
                q_man |= Q(manufacturer=man)

            qs_filters.append(q_man)

        # Create Q objects for category specific checkbox filters
        category_params = dict()

        for key in self.get_specs():
            category_params[key] = self.request.POST.getlist('filters[{}][]'.format(key))

        for key in category_params:
            if category_params[key]:
                q_obj = self.generate_q_filters(key, category_params[key])
                qs_filters.append(q_obj)

        # Filter the QuerySet with the list of Q objects
        qs = qs.filter(*qs_filters)

        return qs

    @staticmethod
    def generate_q_filters(key, value):
        q_obj = Q()

        for val in value:
            q_obj |= Q(specs__contains={key: val})

        return q_obj

    def ordering(self, qs):
        """ Get parameters from the request and prepare order by clause
        """

        # Number of columns that are used in sorting
        sorting_cols = 0
        if self.pre_camel_case_notation:
            try:
                sorting_cols = int(self._querydict.get('iSortingCols', 0))
            except ValueError:
                sorting_cols = 0
        else:
            sort_key = 'order[{0}][column]'.format(sorting_cols)
            while sort_key in self._querydict:
                sorting_cols += 1
                sort_key = 'order[{0}][column]'.format(sorting_cols)

        order = []
        order_columns = self.get_order_columns()

        for i in range(sorting_cols):
            # sorting column
            sort_dir = 'asc'
            try:
                if self.pre_camel_case_notation:
                    sort_col = int(self._querydict.get('iSortCol_{0}'.format(i)))
                    # sorting order
                    sort_dir = self._querydict.get('sSortDir_{0}'.format(i))
                else:
                    sort_col = int(self._querydict.get('order[{0}][column]'.format(i)))
                    # sorting order
                    sort_dir = self._querydict.get('order[{0}][dir]'.format(i))
            except ValueError:
                sort_col = 0

            sdir = '-' if sort_dir == 'desc' else ''
            sortcol = order_columns[sort_col]

            if isinstance(sortcol, list):
                for sc in sortcol:
                    order.append('{0}{1}'.format(sdir, sc.replace('.', '__')))
            else:
                # HACK As of Django 1.9, order_by() cannot be directly called on jsonfield keys
                # This is a workaround using RawSQL() objects
                if sortcol.startswith('specs__'):
                    spec = sortcol.split('specs__')[1]

                    # Sort descending
                    if sdir:
                        sortcol = RawSQL("specs->>%s", (spec,)).desc()

                    else:
                        sortcol = RawSQL("specs->>%s", (spec,))

                    order.append(sortcol)

                else:
                    order.append('{0}{1}'.format(sdir, sortcol.replace('.', '__')))

        if order:
            return qs.order_by(*order)

        return qs

    def output_items(self, item):
        # Fill in the generic item columns and insert category specific column data starting at index 1

        part_data = [
            '<b><a href="{}">{}</a></b>'.format(item.get_absolute_url(), str(item)),
            # Insert specific category columns at index 1
            '<span class="star-rating" data-rating="{}" data-total="{}"><span>'.format(item.rating_avg, len(item.active_reviews)),
            item.get_price(),
            '<a class="btn btn-sm btn-default category-add-button" onclick="addItem({});"><i class="fa fa-plus fa-fw" aria-hidden="true"></i> Add</a>'.format(item.id)
        ]

        for column in self.get_part_columns()[::-1]:
            part_data.insert(1, item.specs.get(column, None))

        return part_data

    def prepare_results(self, qs):
        json_data = list()

        for item in qs:
            json_data.append(self.output_items(item))

        return json_data
