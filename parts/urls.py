from django.conf.urls import url

from parts import views

urlpatterns = [
    url(r'^$', views.CategoryAllView.as_view(), name='category_list_all'),
    url(r'^cat/(?P<category>[a-z0-9_]+)/$', views.PartCategoryView.as_view(), name='parts_list_category'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/(?P<title>[0-9a-z-]+)/$', views.part_detail, name='parts_detail'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/(?P<title>[0-9a-z-]+)/reviews/$', views.PartReviewList.as_view(),
        name='parts_review_list'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/(?P<title>[0-9a-z-]+)/reviews/(?P<review_id>[0-9]+)/$', views.part_review_detail,
        name='parts_review_detail'),
    url(r'^(?P<slug>[A-Za-z0-9]{7})/vendor/(?P<vendor>[0-9A-Z]+)/$', views.part_vendor, name='parts_vendor'),
    url(r'^review/(?P<review_id>[0-9]+)/rate$', views.review_vote, name='review_vote'),
    url(r'^dt/(?P<category>[a-z0-9_]+)/$', views.PartsCategoryDT.as_view(), name='parts_dt'),
]
